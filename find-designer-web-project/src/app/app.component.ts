import { Component } from '@angular/core';
import {AuthService} from './service/auth-service/auth.service';
import {Router} from '@angular/router';
import {ChatService} from './service/chat-service/chat.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public isLoading: boolean = false;

  constructor(private authService: AuthService, private router: Router, private chatService: ChatService){
    this.isLoading = true;
    this.connectToChat();
    this.checkUser();
  }

  checkUser(){
    this.isLoading = true;
    this.authService.isUserLoggedIn().subscribe(res =>{
      if(res.id != -1 && res.id != 1){
        console.log(res);
        let user = this.authService.parseUser(res);
        this.authService.setLoggedUser(user);
        this.router.navigate(['/projects']);
      }else{
        this.authService.loginDefaultUser().subscribe(() => {
          this.router.navigate(['/projects']);
        });
      }
      this.isLoading = false;
    });
  }

  connectToChat(){
    this.chatService.connect();
  }
}
