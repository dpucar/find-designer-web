import {Component} from '@angular/core';
import {ProjectService} from '../../service/project-service/project.service';
import {ProjectType} from '../../model/model/projectType';
import {designAddress} from '../../service/design.path';
import {UserService} from '../../service/user-service/user.service';
import {Router} from '@angular/router';
import {AuthService} from '../../service/auth-service/auth.service';
import {User} from '../../model/model/user';
import {Project} from '../../model/model/project';

@Component({
  selector: 'app-add-project',
  templateUrl: 'add-project.component.html',
  styleUrls: ['add-project.component.scss']
})
export class AddProjectComponent {
  user: User;
  project: Project;
  isLoading: boolean = false;
  projectTypes: ProjectType[] = [];

  defaultProjectTypeMessage: string = "Choose project type";
  projectName: string = "";
  projectDescritpion: string = "";
  projectType: ProjectType = null;

  projectPictures: string[]= [];
  fileToUpload: File[] = [];

  enabled: boolean = false;

  constructor(private projectService: ProjectService, private userService: UserService, private router: Router, private authService: AuthService){
    this.isLoading = true;
    this.user = this.authService.getLoggedUser();
    this.projectService.getAllProjectTypes().subscribe(res => {
      this.projectTypes = this.projectService.parseProjectTypes(res);
      this.isLoading = false;
    });

  }

  setNewProjectType(pr: ProjectType){
    this.projectType = pr;
    this.defaultProjectTypeMessage = pr.name;
  }

  addNewPicture(fileInput){
    this.fileToUpload = <Array<File>> fileInput.target.files;
  }

  savePictures(){
    this.userService.makeFileRequest(designAddress + "/pictureUpload/projectPicture", [], this.fileToUpload).then((result: any) => {
      this.projectPictures.push(result);
    });
  }

  removePicture(picture){
    let strArray: string[] = [];

    this.projectPictures.forEach(pr => {
      if(picture != pr){
        strArray.push(pr);
      }
    });

    this.projectPictures = strArray;
  }

  createProject(event){
    this.isLoading = true;
    this.projectService.saveProject(this.projectName, this.projectDescritpion, this.projectType).subscribe(res => {
      console.log(res);
      this.project = res;
      this.projectService.saveProjectPictures(res, this.projectPictures).subscribe(res => {
        this.projectService.saveUserProject(this.user, this.project).subscribe(res => {
          this.isLoading = false;
          this.router.navigate(['/my-projects']);
        });
      });
    });
  }
}
