import {Component} from '@angular/core';
/**
 * Created by Domagoj on 6.5.2017..
 */
declare let jQuery;
@Component({
  selector: 'app-footer',
  templateUrl: 'footer.component.html',
  styleUrls: ['footer.component.scss']
})
export class FooterComponent {
  constructor(){

  }

  goTo(event){
    if(event == 'toLogin'){
      jQuery("#loginModal").modal("show");
      jQuery("#registerModal").modal("hide");
    }else if(event == 'toRegister'){
      jQuery("#loginModal").modal("hide");
      jQuery("#registerModal").modal("show");
    }
  }
}
