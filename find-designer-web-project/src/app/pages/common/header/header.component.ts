import {Component, Input} from '@angular/core';
import {User} from '../../../model/model/user';
import {AuthService} from '../../../service/auth-service/auth.service';
import {NavigationEnd, Router} from '@angular/router';
import {ProjectService} from '../../../service/project-service/project.service';
/**
 * Created by Domagoj on 6.5.2017..
 */

declare let jQuery:any;
@Component({
  selector: 'app-header',
  templateUrl : 'header.component.html',
  styleUrls: ['header.component.scss']
})
export class HeaderComponent {
  loggedUser: User = null;
  isProjectsOpen: boolean = true;

  constructor(private authService: AuthService, private router: Router, private projectService: ProjectService){
    this.authService.userLoggedInEvent.subscribe(u => {
      this.setUser(u);
    });

    this.loggedUser = this.authService.getLoggedUser();

    this.router.events.subscribe(event => {
      if(event instanceof NavigationEnd){
        if(event.url != '/projects'){
          this.isProjectsOpen = false;
        }else{
          this.isProjectsOpen = true;
        }
      }
    });
  }

  goTo(event){
    if(event == 'toLogin'){
      jQuery("#loginModal").modal("show");
      jQuery("#registerModal").modal("hide");
    }else if(event == 'toRegister'){
      jQuery("#loginModal").modal("hide");
      jQuery("#registerModal").modal("show");
    }
  }

  setUser(user){
    jQuery("#loginModal").modal("hide");
    jQuery("#registerModal").modal("hide");
    this.loggedUser = user;
  }

  searchProjects(term: string){
    this.projectService.searchedTerm.emit(term);
  }

  logout(){
    this.authService.logout().subscribe(res => {
      console.log(res);
      this.authService.setLoggedUser(null);
      this.authService.loginDefaultUser().subscribe(res => {
        this.router.navigate(['/projects']);
      });
    });
  }
}
