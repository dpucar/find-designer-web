/**
 * Created by Domagoj on 16.5.2017..
 */
import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AuthService} from '../../service/auth-service/auth.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
@Component({
  selector: 'app-login',
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.scss']
})
export class LoginComponent implements OnInit{
  @Output() c = new EventEmitter();
  loginForm: FormGroup;
  isLoading: boolean = false;
  hasError: boolean = false;

  constructor(private authService: AuthService){

  }

  ngOnInit(){
    this.loginForm = new FormGroup({
      'email': new FormControl(null, [Validators.required, Validators.email]),
      'password': new FormControl(null, Validators.required)
    });
  }

  goToRegister(){
    this.c.emit('toRegister');
  }

  login(){
    this.isLoading = true;
    this.authService.login(this.loginForm.value.email, this.loginForm.value.password).subscribe(res =>{
      console.log(res);
      if(res.id == -1){
        this.isLoading = false;
        this.hasError = true;
      }else{
        console.log(this.authService.parseUser(res));
        this.loginForm.reset();
        this.authService.setLoggedUser(this.authService.parseUser(res));
        this.isLoading = false;
      }
    });
  }
}
