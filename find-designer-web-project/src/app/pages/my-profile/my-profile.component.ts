import {Component, OnInit} from '@angular/core';
import {User} from '../../model/model/user';
import {AuthService} from '../../service/auth-service/auth.service';
import {UserService} from '../../service/user-service/user.service';
import {designAddress} from '../../service/design.path';
/**
 * Created by Domagoj on 19.6.2017..
 */
@Component({
  selector: 'app-my-profile',
  templateUrl: 'my-profile.component.html',
  styleUrls: ['my-profile.component.scss']
})
export class MyProfileComponent implements OnInit{
  user: User;
  password: string = "";
  repeatPassword: string = "";

  filetoUpload: Array<File>;
  isUploading: boolean = false;

  constructor(private authService: AuthService, private userService: UserService){

  }

  ngOnInit() {
    this.user = this.authService.getLoggedUser();
  }

  fileChanged(fileInput: any){
    console.log('File changed');
    this.filetoUpload = <Array<File>> fileInput.target.files;
  }

  updateUser(){
    console.log("Pozvan sam!");
    if(this.password != ""){
      this.user.password = this.password;
    }

    this.isUploading = true;
    if(this.filetoUpload != null){
      this.userService.makeFileRequest(designAddress + "/pictureUpload/profilePicture", [], this.filetoUpload).then((result: any) => {
        this.isUploading = false;

        console.log(result);
        this.user.userPicture = result;

        this.updateCurrentUser();
      }, (error) => {
        console.error(error);
        this.isUploading = false;
      });
    }else{
      this.updateCurrentUser();
    }
  }

  updateCurrentUser(){
    this.userService.updateUser(this.user).subscribe(res => {
      console.log(res);
      this.isUploading = false;
      this.authService.setLoggedUser(res);
      this.user = res;

      this.password = "";
      this.repeatPassword = "";
    }, (error) => {
      this.isUploading = false;
    });
  }
}
