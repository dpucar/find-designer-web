import {Component, Input, OnInit} from '@angular/core';
import {UserProject} from '../../model/model/userProject';
import {ReviewService} from '../../service/review-service/review.service';
import {User} from '../../model/model/user';
import {AuthService} from '../../service/auth-service/auth.service';
import {ProjectService} from '../../service/project-service/project.service';
import {Router} from '@angular/router';
/**
 * Created by Domagoj on 15.5.2017..
 */
@Component({
  selector: 'app-project-details',
  templateUrl: 'project-details.component.html',
  styleUrls: ['project-details.component.scss']
})
export class ProjectDetailsComponent {
  @Input() userProject: UserProject;
  myReview: string = "";
  loggedUser: User = null;
  reciever: User = null;

  constructor(private authService: AuthService, private reviewService: ReviewService) {
    this.loggedUser = this.authService.getLoggedUser();

    this.authService.userLoggedInEvent.subscribe(res => {
      this.loggedUser = res;
    });
  }

  addReview(){
    if(this.myReview.length < 15){
      //show alert
    }else{
      this.reviewService.saveReview(new Object({dateCreated: new Date, review: this.myReview, user: this.loggedUser})).subscribe(res =>{
        let review = this.reviewService.parseReview(res);
        this.userProject.project.reviews.push(review);
        this.myReview = "";
        this.reviewService.saveProjectReview(new Object({project: this.userProject.project, review: review})).subscribe(res =>{
          console.log(res);
        });
      });
    }
  }

  setRecieverId(reciever){
    this.reciever = reciever;
  }
}
