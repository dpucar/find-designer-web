/**
 * Created by Domagoj on 31.7.2017..
 */
import {Component} from '@angular/core';
import {AuthService} from '../../../service/auth-service/auth.service';
import {User} from '../../../model/model/user';
import {Project} from '../../../model/model/project';
import {ProjectService} from '../../../service/project-service/project.service';
import {Post} from '../../../model/model/post';
import {PostService} from '../../../service/post-service/post.service';

declare let jQuery:any;

@Component({
  selector: 'app-add-post-modal',
  templateUrl: 'add-post-modal.component.html',
  styleUrls: ['add-post-modal.component.scss']
})
export class AddPostModalComponent {
  isLoading: boolean = false;
  dropodownTitle: string = "Choose project";
  showErrors: boolean = false;

  postTitle: string = "";
  postContent: string = "";

  loggedUser: User = null;

  projects: Project[] = [];
  selectedProjectId: number = null;

  constructor(private authService: AuthService, private projectService: ProjectService, private postService: PostService){
    this.isLoading = true;
    this.loggedUser = this.authService.getLoggedUser();

    if(this.loggedUser != null){
      this.projectService.getProjectsByUserId(this.loggedUser.id).subscribe(res => {
        this.projects = this.projectService.parseProjects(res);
        this.isLoading = false;
      });
    }
  }

  setSelectedProject(pr: Project){
    this.dropodownTitle = pr.name;
    this.selectedProjectId = pr.id;
  }

  addPost(){
    if(this.selectedProjectId == null){
      this.showErrors = true;
    }else{
      let post = new Post(this.postTitle, this.postContent, new Date(), this.selectedProjectId);
      this.postService.savePost(post).subscribe(res => {
        let userProject = new Object({user: this.loggedUser, post: res});
        this.postService.saveUserPost(userProject).subscribe(() => {
          this.dropodownTitle = "Choose project";
          this.selectedProjectId = null;
          this.postService.reloadPosts.emit(true);
          jQuery('#add-post-modal').modal('hide');
        });
      });
    }

  }
}
