import {Component} from '@angular/core';
import {PostService} from '../../service/post-service/post.service';
import {UserPost} from '../../model/model/userPost';
import {UserProject} from '../../model/model/userProject';
import {ProjectService} from '../../service/project-service/project.service';
import {User} from '../../model/model/user';
import {AuthService} from '../../service/auth-service/auth.service';
/**
 * Created by Domagoj on 6.5.2017..
 */
@Component({
  selector: 'app-news',
  templateUrl: 'posts.component.html',
  styleUrls: ['posts.component.scss']
})
export class PostComponent {
  isLoading: boolean = false;
  userPosts: UserPost[] = [];
  selectedProject: UserProject = null;
  loggedUser: User = null;

  constructor(private postService: PostService, private projectService: ProjectService, private authService: AuthService) {
    this.isLoading = true;
    this.loggedUser = this.authService.getLoggedUser();

    this.loadPosts();

    this.postService.reloadPosts.subscribe(() => {
      this.isLoading = true;
      this.loadPosts();
    });

    this.authService.userLoggedInEvent.subscribe(res => {
      this.loggedUser = res;
    });

  }

  loadPosts(){
    this.postService.getAllUserPosts().subscribe(res => {
      this.userPosts = this.postService.parseUserPost(res);
      this.isLoading = false;
    });
  }

  setProjectById(id: number){
    this.isLoading = true;
    this.projectService.getUserProjectByProjectId(id).subscribe(res => {
      this.isLoading = false;
      this.selectedProject = this.projectService.parseUserProject(res);
    });
  }
}
