/**
 * Created by Domagoj on 5.8.2017..
 */
import {Component, Input} from '@angular/core';
import {ChatService} from '../../../service/chat-service/chat.service';
import {UUID} from 'angular2-uuid';
import {Message} from '../../../model/model/message';
import {AuthService} from '../../../service/auth-service/auth.service';
import {Router} from '@angular/router';
import {User} from '../../../model/model/user';

@Component({
  selector: 'app-add-new-messages',
  templateUrl: 'add-new-messages.component.html',
  styleUrls: ['add-new-messages.component.scss']
})
export class AddNewMessagesComponent {
  @Input() reciever: User;
  message: string = "";
  messageTitle: string = "";

  constructor(private chatService: ChatService, private authService: AuthService, private router: Router){
  }

  cleanMessage(){
    this.message = "";
    this.messageTitle = "";
  }

  sendMessage(){
    console.log(UUID.UUID());
    this.chatService.checkIfChatroomWithUserExists(this.reciever.id).subscribe(res => {
      //if not exists create one
      //if exists send message to existing one
      //after sending go to my messages
      if(res.id == null){
        //create new chatroom and send message
        this.chatService.sendMessage(new Message(new Date(), this.authService.getLoggedUser(), this.reciever, UUID.UUID(), this.messageTitle, this.message));
        this.cleanMessage();
        // this.router.navigate(['/my-messages']);
      }else{
        //send message to existing one
        this.chatService.sendMessage(new Message(new Date(), this.authService.getLoggedUser(), this.reciever, res.chatroom, res.chatroomTitle, this.message));
        this.cleanMessage();
        // this.router.navigate(['/my-messages']);
      }
    });
  }
}
