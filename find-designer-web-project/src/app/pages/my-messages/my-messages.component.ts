import {AfterViewChecked, Component, ElementRef, ViewChild} from '@angular/core';
import {ChatService} from '../../service/chat-service/chat.service';
import {AuthService} from '../../service/auth-service/auth.service';
import {DisplayMessageList} from '../../model/model/displayMessageList';
import {Message} from '../../model/model/message';
import {User} from '../../model/model/user';
import {forEach} from '@angular/router/src/utils/collection';
/**
 * Created by Domagoj on 21.6.2017..
 */
@Component({
  selector: 'app-my-messages',
  templateUrl: 'my-messages.component.html',
  styleUrls: ['my-messages.component.scss']
})
export class MyMessagesComponent {
  loggedUser: User = null;
  displayMessages: DisplayMessageList[] = [];
  messages: Message[] = [];
  chatroom: string = null;
  chatroomTitle: string = null;
  reciever: User = null;
  message: string = "";
  isSelected: boolean = false;
  selectedChatroom: string = null;

  constructor(private chatService: ChatService, private authService: AuthService){
    this.loggedUser = this.authService.getLoggedUser();

    this.chatService.getChatroomsForUser(this.loggedUser.id).subscribe(res => {
      console.log(res);
      this.displayMessages = this.chatService.parseDisplayMessageList(res);
    });


    this.chatService.messageEmitter.subscribe(res => {
      this.messages.push(res);
    });
  }

  setSelectedChat(chatroomId){
    if(this.selectedChatroom != null){
      this.chatService.unsubscribeFromChatroom();
    }

    this.selectedChatroom = chatroomId;

    this.chatService.getChatFromChatroom(this.selectedChatroom).subscribe(res => {
      this.messages = this.chatService.parseMessagesObject(res);

      this.chatroom = this.messages[0].chatroom;
      this.chatroomTitle = this.messages[0].chatroomTitle;
      this.reciever = this.messages[0].reciever;

      this.isSelected = true;
    });

    this.chatService.subscribeToChatroom(this.selectedChatroom);
  }

  sendMessage(){
    if(this.isSelected){
      this.chatService.sendMessage(new Message(new Date(), this.loggedUser, this.reciever, this.chatroom, this.chatroomTitle, this.message));
      this.message = "";
    }
  }
}
