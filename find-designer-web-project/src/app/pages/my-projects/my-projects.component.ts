/**
 * Created by Domagoj on 20.6.2017..
 */
import {Component} from '@angular/core';
import {ProjectService} from '../../service/project-service/project.service';
import {Project} from '../../model/model/project';
import {AuthService} from '../../service/auth-service/auth.service';
import {User} from '../../model/model/user';
import {Router} from '@angular/router';
@Component({
  selector: 'app-my-projects',
  templateUrl: 'my-projects.component.html',
  styleUrls: ['my-projects.component.scss']
})
export class MyProjectsComponent {
  isLoading: boolean = false;
  myProjects: Project[] = [];
  user: User = null;

  constructor(private projectService: ProjectService, private authService: AuthService, private router: Router){
    this.isLoading = true;

    this.user = this.authService.getLoggedUser();
    this.projectService.getProjectsByUserId(this.user.id).subscribe(res => {
      console.log(res);
      this.myProjects = this.projectService.parseProjects(res);
      this.isLoading = false;
    });
  }

  openAddProject(){
    this.router.navigate(['/add-new-project']);
  }
}
