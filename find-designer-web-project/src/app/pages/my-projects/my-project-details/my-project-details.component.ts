import {Component, Input} from '@angular/core';
import {Project} from '../../../model/model/project';
import {ProjectService} from '../../../service/project-service/project.service';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../../../service/user-service/user.service';
import {designAddress} from '../../../service/design.path';
import {ProjectPictures} from '../../../model/model/projectPictures';
import {ProjectType} from '../../../model/model/projectType';
/**
 * Created by Domagoj on 21.6.2017..
 */
@Component({
  selector: 'app-my-project-details',
  templateUrl: 'my-project-details.component.html',
  styleUrls: ['my-project-details.component.scss']
})
export class MyProjectDetilsComponent {
  id: number = null;
  project: Project = null;
  isLoading: boolean = false;

  projectTypes: ProjectType[] = [];

  newProjectPictures: string[] = [];
  fileToUpload: File[] = [];
  isUploading: boolean = false;

  constructor(private activatedRoute: ActivatedRoute, private projectService: ProjectService, private userService: UserService, private router: Router){
    this.id = +this.activatedRoute.snapshot.params['id'];
    this.isLoading = true;
    this.projectService.getProjectById(this.id).subscribe(res => {
      this.project = this.projectService.parseProject(res);

      this.projectService.getAllProjectTypes().subscribe(res => {
        this.projectTypes = this.projectService.parseProjectTypes(res);
        this.isLoading = false;
      });
    });
  }

  changeEnableDisable(){
    this.project.enabled = !this.project.enabled;
  }

  addNewPicture(fileInput){
    this.fileToUpload = <Array<File>> fileInput.target.files;
  }

  savePictures(){
    this.userService.makeFileRequest(designAddress + "/pictureUpload/projectPicture", [], this.fileToUpload).then((result: any) => {

      console.log(result);
      this.newProjectPictures.push(result);
    });
  }

  removeNewPicture(picture){
    let strArray: string[] = [];

    this.newProjectPictures.forEach(pr => {
      if(picture != pr){
        strArray.push(pr);
      }
    });

    this.newProjectPictures = strArray;
  }

  removeOldPicture(picture){
    let strArray: ProjectPictures[] = [];

    this.project.projectPictures.forEach(pr => {
      if(picture != pr){
        strArray.push(pr);
      }
    });

    this.project.projectPictures = strArray;
  }

  updateProject(){
    this.isLoading = true;
    this.projectService.updateProject(this.project).subscribe(res => {
      console.log(res);
      this.projectService.saveProjectPictures(this.project, this.newProjectPictures).subscribe(res => {
        console.log(res);
        this.isLoading = false;
        this.router.navigate(['my-projects']);
      });
    });
  }

  goBackToMyProjects(){
    this.router.navigate(['my-projects']);
  }
}
