/**
 * Created by Domagoj on 6.5.2017..
 */
import {Component} from "@angular/core";
@Component({
  selector: 'app-spinner',
  template: `
    <div style="width: 100%; height: 100%; text-align: center;">
      <i style="margin-top: calc(50vh - 40px)" class="fa fa-spinner fa-pulse fa-5x"></i>
    </div>
  `
})
export class SpinnerComponent{

}
