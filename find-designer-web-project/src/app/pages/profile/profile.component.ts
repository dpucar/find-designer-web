import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ProjectService} from '../../service/project-service/project.service';
import {UserProject} from '../../model/model/userProject';
import {User} from '../../model/model/user';
/**
 * Created by Domagoj on 21.6.2017..
 */
@Component({
  selector: 'app-profile',
  templateUrl: 'profile.component.html',
  styleUrls: ['profile.component.scss']
})
export class ProfileComponent {
  isLoading: boolean = false;
  userProjects: UserProject[] = [];
  id: number = null;
  selectedProject: UserProject = null;

  constructor(private activatedRoute: ActivatedRoute, private projectService: ProjectService){
    this.isLoading = true;
    this.id = +this.activatedRoute.snapshot.params['id'];
    this.projectService.getUserProjectsByUserId(this.id).subscribe(res => {
      this.userProjects = this.projectService.parseActiveUserProjects(res);
      this.isLoading = false;
    });
  }

  setProject(userProjects: UserProject){
    this.selectedProject = userProjects;
  }
}
