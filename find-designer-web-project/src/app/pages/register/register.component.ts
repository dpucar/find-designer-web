/**
 * Created by Domagoj on 16.5.2017..
 */
import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AuthService} from '../../service/auth-service/auth.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
@Component({
  selector: 'app-register',
  templateUrl: 'register.component.html',
  styleUrls: ['register.component.scss']
})
export class RegisterComponent implements OnInit{
  @Output() c = new EventEmitter();
  userTypes = ['User', 'Designer'];
  registerForm: FormGroup;
  isLoading: boolean = false;

  constructor(private authService: AuthService){

  }

  ngOnInit(){
    this.registerForm = new FormGroup({
      'name' : new FormControl(null, Validators.required),
      'surname' : new FormControl(null, Validators.required),
      'email': new FormControl(null, [Validators.required, Validators.email]),
      'password': new FormControl(null, [Validators.required, Validators.minLength(4)]),
      'repeatPassword': new FormControl(null, [Validators.required]),
      'type': new FormControl('User')
    });
  }

  arePasswordsSame(control: FormControl): {[message: string]: boolean}{
    if(!control.value != this.registerForm.value.password){
      return {'notSamePasswords': true};
    }
    return null;
  }

  //asny validacija
  doesEmailExists(control: FormControl): Promise<any> | Observable<any>{
    const promise = new Promise<any>((resolve, reject) => {
      this.authService.doesEmailExists(control.value).subscribe(res => {
        console.log(res);
        if(res == true){
          resolve({'emailIsInUse': true});
        }else{
          resolve(null);
        }
      });
    });

    return promise;
  }

  register(){
    this.isLoading = true;
    let user = new Object({email: this.registerForm.value.email, password: this.registerForm.value.password, name: this.registerForm.value.name,
      surname: this.registerForm.value.surname, enabled: 1, dateCreated: new Date()});
    this.authService.register(user, this.registerForm.value.type.toUpperCase()).subscribe(res => {
      this.authService.login(this.registerForm.value.email, this.registerForm.value.password).subscribe(res => {
        console.log(res);
        this.authService.setLoggedUser(this.authService.parseUser(res));
        this.registerForm.reset();
        this.isLoading = false;
      });
    });
  }

  goToLogin(){
    this.c.emit('toLogin');
  }
}
