import {Component} from '@angular/core';
import {ProjectService} from '../../service/project-service/project.service';
import {UserProject} from '../../model/model/userProject';
import {Project} from '../../model/model/project';
/**
 * Created by Domagoj on 6.5.2017..
 */
@Component({
  selector: 'app-projects',
  templateUrl: 'projects.component.html',
  styleUrls: ['projects.component.scss']
})
export class ProjectsComponent {
  isLoading = false;
  userProjects: UserProject[] = [];
  tmpUserProjects: UserProject[] = [];
  selectedProject: UserProject = null;

  details: boolean = false;
  overProjectId: number = -1;

  constructor(private _projectService: ProjectService){
    this.isLoading = true;
    this._projectService.getUserProjects().subscribe(res => {
      this.userProjects = this._projectService.parseActiveUserProjects(res);
      this.tmpUserProjects = this.userProjects;
      console.log(this.userProjects);
      this.isLoading = false;
    });


    this._projectService.searchedTerm.subscribe(searchedTerm => {
      this.searchProjects(searchedTerm);
    });

  }

  setProject(userProjects: UserProject){
    this.selectedProject = userProjects;
  }

  searchProjects(term: string){
    this.userProjects = [];
    if(term != '' || term != null){
      for(let i=0; i < this.tmpUserProjects.length; i++){
        if(this.tmpUserProjects[i].project.name.toLowerCase().indexOf(term.toLowerCase()) >-1
          || this.tmpUserProjects[i].project.projectType.name.toLowerCase().indexOf(term.toLowerCase()) >-1
          || this.tmpUserProjects[i].user.name.toLowerCase().indexOf(term.toLowerCase()) > -1
        ){
          this.userProjects.push(this.tmpUserProjects[i]);
        }
      }
    }
    else {
      this.userProjects = this.tmpUserProjects;
    }
  }

  showDetails(id){
    this.details = true;
    this.overProjectId = id;
  }

  removeDetails(){
    this.details = false;
    this.overProjectId = -1;
  }
}
