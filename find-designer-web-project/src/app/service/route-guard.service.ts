import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {Injectable} from '@angular/core';
import {AuthService} from './auth-service/auth.service';
/**
 * Created by Domagoj on 19.6.2017..
 */
@Injectable()
export class RouteGuard implements CanActivate{

  constructor(private authService: AuthService){

  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if(this.authService.getLoggedUser() != null){
      return true;
    }
    return false;
  }
}
