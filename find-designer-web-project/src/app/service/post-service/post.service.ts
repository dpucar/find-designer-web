import {EventEmitter, Injectable} from '@angular/core';
import {designAddress} from '../design.path';
import {Headers, Http, RequestOptions} from '@angular/http';
import {Post} from '../../model/model/post';
import {UserPost} from '../../model/model/userPost';
import {UserService} from '../user-service/user.service';
/**
 * Created by Domagoj on 21.6.2017..
 */
@Injectable()
export class PostService{
  private address: string = designAddress + '/posts';

  public reloadPosts: EventEmitter<any>;

  constructor(private http: Http, private userService: UserService){
    this.reloadPosts = new EventEmitter();
  }

  savePost(post: Post){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    let body = JSON.stringify(post);

    return this.http.post(this.address + '/post', body, options).map(res => res.json());
  }

  saveUserPost(userPost: any){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    let body = JSON.stringify(userPost);

    return this.http.post(this.address + '/userPosts', body, options).map(res => res.json());
  }

  getAllUserPosts(): any{
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });

    return this.http.get(this.address + '/userPosts', options).map(res => res.json());
  }

  parseUserPost(res: any): UserPost[]{
    let userPost: UserPost[] = [];
    for(let up of res){
      userPost.push(new UserPost(up.id, this.userService.parseUser(up.user), this.parsePost(up.post)));
    }

    return userPost;
  }

  parsePost(p: any): Post{
    return new Post(p.title, p.text, p.dateCreated, p.projectId);
  }

  parsePosts(res: any): Post[]{
    let posts: Post[] = [];

    for(let p of res){
      posts.push(new Post(p.title, p.text, p.dateCreated, p.projectId));
    }

    return posts;
  }
}
