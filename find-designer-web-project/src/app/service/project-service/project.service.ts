/**
 * Created by Domagoj on 6.5.2017..
 */
import 'rxjs/add/operator/map';
import {Headers, Http, RequestOptions} from '@angular/http';
import {EventEmitter, Injectable} from '@angular/core';
import {Review} from '../../model/model/review';
import {ProjectPictures} from '../../model/model/projectPictures';
import {Project} from '../../model/model/project';
import {designAddress} from '../design.path';
import {UserProject} from '../../model/model/userProject';
import {UserService} from '../user-service/user.service';
import {ProjectType} from '../../model/model/projectType';
import {User} from '../../model/model/user';

@Injectable()
export class ProjectService {
  private projectAddress = designAddress + '/projects';
  private userProjectAddress = designAddress + '/userProjects';
  private projectTypeAddress = designAddress + '/projectType';
  private projectPicturesAddress = designAddress + '/projectPicutres';

  searchedTerm: EventEmitter<string>;

  constructor(private _http: Http, private userService: UserService){
    this.searchedTerm = new EventEmitter<string>();
  }

  getUserProjects(): any{
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });

    return this._http.get(this.userProjectAddress, options).map(res => res.json());
  }

  getUserProjectByProjectId(id: number): any{
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });

    return this._http.get(this.userProjectAddress + '/byproject/' + id, options).map(res => res.json());
  }

  getUserProjectsByUserId(id: number): any{
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });

    return this._http.get(this.userProjectAddress + '/byuser/' + id, options).map(res => res.json());
  }

  getProjectsByUserId(id: number): any{
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });

    return this._http.get(this.userProjectAddress + '/' + id, options).map(res => res.json());
  }

  getProjects(): any{
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });

    return this._http.get(this.projectAddress, options).map(res => res.json());
  }

  getProjectById(id: number){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });

    return this._http.get(this.projectAddress + '/' + id, options).map(res => res.json());
  }

  getAllProjectTypes(){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });

    return this._http.get(this.projectTypeAddress, options).map(res => res.json());
  }

  saveProject(projectName: string, projectDescription: string, projectType: ProjectType){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });

    let body = JSON.stringify({name: projectName, description: projectDescription, dateCreated: new Date(), projectType: projectType,
      likeNumber: 0, viewNumber: 0, reviewNumber: 0, enabled: true});

    return this._http.post(this.projectAddress, body, options).map(res => res.json());
  }

  saveUserProject(user: User, project: Project){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });

    let body = JSON.stringify({user: user,project: project});

    return this._http.post(this.userProjectAddress, body, options).map(res => res.json());
  }

  saveProjectPictures(project: Project, pp: string[]){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });

    let projectPictures: Object[] = [];

    for(let p of pp){
      projectPictures.push(new Object({link: p, project: project}));
    }

    let body = JSON.stringify(projectPictures);

    console.log(body);

    return this._http.post(this.projectPicturesAddress + '/pictures', body, options).map(res => res.json());
  }

  updateProject(project: Project) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });

    let body = JSON.stringify(project);

    return this._http.put(this.projectAddress + '/' + project.id, body, options).map(res => res.json());
  }

  parseProjectTypes(res: any[]): ProjectType[]{
    let projectTypes: ProjectType[] = [];

    for(let pr of res){
      projectTypes.push(new ProjectType(pr.id, pr.name));
    }

    return projectTypes;
  }

  parseProject(res: any): Project{
    let reviews: Review[] = [];
    let projectPictures: ProjectPictures[] = [];

    for(let r of res.reviews){
      reviews.push(new Review(r. id, r.review, r.dateCreated, this.userService.parseUser(r.user)));
    }

    for(let pp of res.projectPictures){
      projectPictures.push(new ProjectPictures(pp.id, pp.link));
    }

    return new Project(res.id, res.name, res.description, res.dateCreated, res.likeNumber, res.reviewNumber, res.viewNumber, reviews, projectPictures, new ProjectType(res.projectType.id, res.projectType.name), res.enabled);
  }

  parseProjects(res: any[]): Project[]{
    let projects: Project[] = [];
    let reviews: Review[];
    let projectPictures: ProjectPictures[];

    for(let p of res){
      projectPictures = [];
      reviews = [];

      for(let r of p.reviews){
        reviews.push(new Review(r.id, r.review, r.dateCreated, this.userService.parseUser(r.user)));
      }

      for(let pp of p.projectPictures){
        projectPictures.push(new ProjectPictures(pp.id, pp.link));
      }

      projects.push(new Project(p.id, p.name, p.description, p.dateCreated, p.likeNumber, p.reviewNumber, p.viewNumber, reviews, projectPictures, new ProjectType(p.projectType.id, p.projectType.name), p.enabled));
    }

    return projects;
  }

  parseActiveUserProjects(res: any[]): UserProject[]{
    let userProjects: UserProject[] = [];

    for(let up of res){
      if(up.project.enabled){
        userProjects.push(new UserProject(up.id, this.userService.parseUser(up.user), this.parseProject(up.project)));
      }
    }

    return userProjects;
  }

  parseUserProject(up: any): UserProject{
    return new UserProject(up.id, this.userService.parseUser(up.user), this.parseProject(up.project));
  }
}
