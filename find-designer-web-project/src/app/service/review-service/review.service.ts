/**
 * Created by Domagoj on 18.6.2017..
 */
import {Injectable} from '@angular/core';
import {designAddress} from '../design.path';
import {Headers, Http, RequestOptions} from '@angular/http';
import {Review} from '../../model/model/review';
import {UserService} from '../user-service/user.service';
@Injectable()
export class ReviewService{
  private address: string = designAddress;

  constructor(private http: Http, private userService: UserService){

  }

  saveProjectReview(pr: any):any{
    let body = JSON.stringify(pr);
    console.log(body);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });

    return this.http.post(this.address + '/projectreviews', body, options).map(res => res.json());
  }

  saveReview(review: any):any{
    let body = JSON.stringify(review);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });

    return this.http.post(this.address + '/reviews', body, options).map(res => res.json());
  }

  parseReview(r: any):any{
    return new Review(r.id, r.review, r.dateCreated, this.userService.parseUser(r.user));
  }
}
