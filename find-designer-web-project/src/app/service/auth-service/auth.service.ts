/**
 * Created by Domagoj on 26.5.2017..
 */
import {EventEmitter, Injectable} from '@angular/core';
import {Headers, Http, RequestOptions} from '@angular/http';
import {User} from '../../model/model/user';
import {designAddress} from '../design.path';
import 'rxjs/add/operator/map';
import {Role} from '../../model/model/role';
import {UserSkill} from '../../model/model/userSkill';
@Injectable()
export class AuthService{
  private address = designAddress;
  private loggedUser: User;
  public userLoggedInEvent: EventEmitter<any>;

  constructor(private http: Http){
    this.userLoggedInEvent = new EventEmitter();
  }

  getLoggedUser(): User{
    return this.loggedUser;
  }

  setLoggedUser(u: User){
    this.loggedUser = u;
    this.userLoggedInEvent.emit(u);
  }

  doesEmailExists(email: any): any{
    let body = JSON.stringify(new Object({email: email}));
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });

    return this.http.post(this.address + '/users/byemail', body, options).map(res => res.json());
  }

  isUserLoggedIn():any{
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });

    return this.http.get(this.address + '/isLoggedIn', options).map(res => res.json());
  }

  login(email: string, password: string){
    let user = {email: email, password: password};
    let body = JSON.stringify(user);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });

    return this.http.post(this.address + '/login', body, options).map(res => res.json());
  }

  loginDefaultUser(){
    let user = {email: '', password: ''};
    let body = JSON.stringify(user);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });

    return this.http.post(this.address + '/loginDefaultUser', body, options).map(res => res.json());
  }

  logout(){
    let body;
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });

    return this.http.post(this.address + '/logout', body, options).map(res => res.json());
  }

  register(user: any, role: string): any{
    let body = JSON.stringify(user);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });

    return this.http.post(this.address + '/users/register/' + role, body, options).map(res => res.json);
  }

  parseUser(res: any): User{
    let userRoles: Role[] = [];
    let userSkills: UserSkill[] = [];

    for(let r of res.roles){
      userRoles.push(new Role(r.id, r.name));
    }

    for(let s of res.userSkills){
      userSkills.push(new UserSkill(s.id, s.name));
    }

    return new User(res.id, res.name, res.surname, res.email, res.password, res.userPicture, res.dateCreated, res.lastUsage ,userRoles, userSkills, res.enabled);
  }
}
