/**
 * Created by Domagoj on 16.5.2017..
 */
import {Injectable} from '@angular/core';
import {Headers, Http, RequestOptions} from '@angular/http';
import {designAddress} from '../design.path';
import 'rxjs/add/operator/map';
import {User} from '../../model/model/user';
import {Role} from '../../model/model/role';
import {UserSkill} from '../../model/model/userSkill';
@Injectable()
export class UserService{
  private address = designAddress + '/users';

  constructor(private http: Http){

  }

  makeFileRequest(url: string, params: Array<string>, files: Array<File>) {
    return new Promise((resolve, reject) => {
      let formData: any = new FormData();
      let xhr = new XMLHttpRequest();
      for(let i =0; i < files.length; i++) {
        formData.append("file", files[i], files[i].name);
      }

      xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            resolve(xhr.response);
          } else {
            reject(xhr.response);
          }
        }
      };
      xhr.open("POST", url, true);
      xhr.send(formData);
    });
  }

  updateUser(user: any): any{
    let body = JSON.stringify(user);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });

    return this.http.post(this.address, body, options).map(res => res.json());
  }

  getUserById(){

  }

  parseUser(res: any): User{
    let userRoles: Role[] = [];
    let userSkills: UserSkill[] = [];

    for(let r of res.roles){
      userRoles.push(new Role(r.id, r.name));
    }

    for(let s of res.userSkills){
      userSkills.push(new UserSkill(s.id, s.name));
    }

    return new User(res.id, res.name, res.surname, res.email, res.password, res.userPicture, res.dateCreated, res.lastUsage ,userRoles, userSkills, res.enabled);
  }
}
