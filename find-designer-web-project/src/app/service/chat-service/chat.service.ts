/**
 * Created by Domagoj on 5.8.2017..
 */
import {EventEmitter, Injectable} from '@angular/core';
import {Headers, Http, RequestOptions} from '@angular/http';
import {chatAddress, designAddress} from '../design.path';
import {AuthService} from '../auth-service/auth.service';
import {Message} from '../../model/model/message';
import {DisplayMessageList} from '../../model/model/displayMessageList';

declare let SockJS: any;
declare let Stomp: any;

@Injectable()
export class ChatService{
  private address = designAddress;

  socket: any;
  stompClient: any;
  messageEmitter = new EventEmitter<Message>();
  connectEmitter = new EventEmitter<boolean>();
  isConnected = false;
  private subscribedRoom: any;

  constructor(private http : Http, private authService: AuthService){
    this.socket = null;
    this.stompClient = null;
  }

  // connect to chat server!
  connect(){
    if(!this.isConnected){
      this.socket = new SockJS(chatAddress + "/chat");
      this.stompClient = Stomp.over(this.socket);
      this.stompClient.connect({}, () => {
        this.isConnected = true;
        this.connectEmitter.emit(true);
      }, () => {
        this.isConnected = false;
      });
    }
  }

  disconnect(): void {
    if(this.isConnected){
      this.stompClient.disconnect();
      this.isConnected = false;
    }
  }

  subscribeToChatroom(chatroomId: string){
    this.subscribedRoom = this.stompClient.subscribe('/topic/chat/room' + chatroomId, (messageOutput) => {
      //incoming messages
      let object = JSON.parse(messageOutput.body);
      console.log(object);

      let message = new Message(object.createdAt, this.authService.parseUser(object.sender), this.authService.parseUser(object.reciever), object.chatroom, object.chatroomTitle, object.message);
      this.messageEmitter.emit(message);
    }, {id: chatroomId});
  }

  unsubscribeFromChatroom(){
    this.subscribedRoom.unsubscribe();
  }

  checkIfChatroomWithUserExists(recieverId){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });

    return this.http.get(this.address + '/checkIfChatroomExists/' + this.authService.getLoggedUser().id + '/' + recieverId, options)
      .map(res => res.json());
  }

  sendMessage(message : Message){
    let body = JSON.stringify(message);

    return this.stompClient.send("/app/chat/room" + message.chatroom, {}, body);
  }

  getChatroomsForUser(userId){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });

    return this.http.get(designAddress + "/getMessagesForUser/" + userId, options)
      .map(res => res.json());
  }

  getChatFromChatroom(chatroom: string){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });

    return this.http.get(chatAddress + "/api/messages/chatroom/" + chatroom, options)
      .map(res => res.json());
  }

  parseMessageObject(msg): Message{
    return new Message(msg.createdAt, this.authService.parseUser(msg.sender), this.authService.parseUser(msg.reciever), msg.chatroom, msg.chatroomTitle, msg.message);
  }

  parseMessagesObject(msg: any[]): Message[]{
    let messages: Message[] = [];

    for(let m of msg){
      messages.push(new Message(m.createdAt, this.authService.parseUser(m.sender), this.authService.parseUser(m.reciever), m.chatroom, m.chatroomTitle, m.message));
    }

    return messages;
  }

  parseDisplayMessageList(res: any[]): DisplayMessageList[] {
    let displayMessages: DisplayMessageList[] = [];

    for(let dm of res){
      displayMessages.push(new DisplayMessageList(dm.chatroomId, dm.chatroomName));
    }

    console.log(displayMessages);

    return displayMessages;
  }
}
