/**
 * Created by Domagoj on 8.8.2017..
 */
export class DisplayMessageList {
  public chatroomId: number;
  public chatroomName: string;


  constructor(chatroomId: number, chatroomName: string) {
    this.chatroomId = chatroomId;
    this.chatroomName = chatroomName;
  }
}
