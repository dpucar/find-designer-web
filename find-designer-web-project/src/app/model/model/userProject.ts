import {Project} from './project';
import {User} from './user';
/**
 * Created by Domagoj on 30.5.2017..
 */
export class UserProject{
  id: number;
  user: User;
  project: Project;


  constructor(id: number, user: User, project: Project) {
    this.id = id;
    this.user = user;
    this.project = project;
  }
}
