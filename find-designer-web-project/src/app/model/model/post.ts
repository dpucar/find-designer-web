export class Post{
  title: string;
  text: string;
  dateCreated: Date;
  projectId: number;


  constructor(title: string, text: string, dateCreated: Date, projectId: number) {
    this.title = title;
    this.text = text;
    this.dateCreated = dateCreated;
    this.projectId = projectId;
  }
}
