/**
 * Created by Domagoj on 21.4.2017..
 */
export class ProjectPictures {
  public id: number;
  public link: string;


  constructor(id: number, link: string) {
    this.id = id;
    this.link = link;
  }
}
