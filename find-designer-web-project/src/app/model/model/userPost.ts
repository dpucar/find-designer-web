import {User} from './user';
import {Post} from './post';
export class UserPost{
  id: number;
  user: User;
  post: Post;

  constructor(id: number, user: User, post: Post) {
    this.id = id;
    this.user = user;
    this.post = post;
  }
}
