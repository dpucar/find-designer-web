import {User} from './user';
/**
 * Created by Domagoj on 21.4.2017..
 */
export class Review {
  public id: number;
  public review: string;
  public dateCreated: Date;
  public user: User;


  constructor(id: number, review: string, dateCreated: Date, user: User) {
    this.id = id;
    this.review = review;
    this.dateCreated = dateCreated;
    this.user = user;
  }
}
