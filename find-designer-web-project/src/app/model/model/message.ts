import {User} from './user';
/**
 * Created by Domagoj on 8.8.2017..
 */
export class Message {
  public createdAt: Date;
  public sender: User;
  public reciever: User;
  public chatroom: string;
  public chatroomTitle: string;
  public message: string;


  constructor(createdAt: Date, sender: User, reciever: User, chatroom: string, chatroomTitle: string, message: string) {
    this.createdAt = createdAt;
    this.sender = sender;
    this.reciever = reciever;
    this.chatroom = chatroom;
    this.chatroomTitle = chatroomTitle;
    this.message = message;
  }
}
