import {Review} from './review';
import {ProjectPictures} from './projectPictures';
import {ProjectType} from './projectType';
/**
 * Created by Domagoj on 21.4.2017..
 */
export class Project {
  public id: number;
  public name: string;
  public description: string;
  public dateCreated: Date;
  public likeNumber: number;
  public reviewNumber: number;
  public viewNumber: number;
  public reviews: Review[];
  public projectPictures: ProjectPictures[];
  public projectType: ProjectType;
  public enabled: boolean;


  constructor(id: number, name: string, description: string, dateCreated: Date, likeNumber: number, reviewNumber: number, viewNumber: number, reviews: Review[], projectPictures: ProjectPictures[], projectType: ProjectType, enabled: boolean) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.dateCreated = dateCreated;
    this.likeNumber = likeNumber;
    this.reviewNumber = reviewNumber;
    this.viewNumber = viewNumber;
    this.reviews = reviews;
    this.projectPictures = projectPictures;
    this.projectType = projectType;
    this.enabled = enabled;
  }
}
