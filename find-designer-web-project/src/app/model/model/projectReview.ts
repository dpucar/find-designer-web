import {Project} from './project';
import {Review} from './review';
/**
 * Created by Domagoj on 18.6.2017..
 */
export class ProjectReview{
  id: number;
  project: Project;
  review: Review;

  constructor(id: number, project: Project, review: Review) {
    this.id = id;
    this.project = project;
    this.review = review;
  }
}
