import {UserSkill} from './userSkill';
import {Role} from './role';
/**
 * Created by Domagoj on 22.5.2017..
 */
export class User{
  id: number;
  name: string;
  surname: string;
  email: string;
  password: string;
  userPicture: string;
  dateCreated: Date;
  lastUsage: Date;
  roles: Role[];
  userSkills: UserSkill[];
  enabled: boolean;


  constructor(id: number, name: string, surname: string, email: string, password: string, userPicture: string, dateCreated: Date, lastUsage: Date, roles: Role[], userSkills: UserSkill[], enabled: boolean) {
    this.id = id;
    this.name = name;
    this.surname = surname;
    this.email = email;
    this.password = password;
    this.userPicture = userPicture;
    this.dateCreated = dateCreated;
    this.lastUsage = lastUsage;
    this.roles = roles;
    this.userSkills = userSkills;
    this.enabled = enabled;
  }
}
