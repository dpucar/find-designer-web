/**
 * Created by Domagoj on 30.5.2017..
 */
export class Role{
  id: number;
  name: string;


  constructor(id: number, name: string) {
    this.id = id;
    this.name = name;
  }
}
