/**
 * Created by Domagoj on 21.4.2017..
 */
export class ProjectType {
  public id: number;
  public name: string;


  constructor(id: number, name: string) {
    this.id = id;
    this.name = name;
  }
}
