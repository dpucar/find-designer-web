import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import {ProjectService} from './service/project-service/project.service';
import {AboutComponent} from './pages/about/about.component';
import {FooterComponent} from './pages/common/footer/footer.component';
import {HeaderComponent} from './pages/common/header/header.component';
import {ProjectsComponent} from './pages/projects/projects.component';
import {RouterModule, Routes} from '@angular/router';
import {ProjectDetailsComponent} from './pages/project-details/project-details.component';
import {CarouselModule} from 'ngx-bootstrap';
import {LoginComponent} from './pages/login/login.component';
import {UserService} from './service/user-service/user.service';
import {RegisterComponent} from './pages/register/register.component';
import {AuthService} from './service/auth-service/auth.service';
import {ReviewService} from './service/review-service/review.service';
import {MyProfileComponent} from './pages/my-profile/my-profile.component';
import {RouteGuard} from './service/route-guard.service';
import {SpinnerComponent} from './pages/help-components/spinner-component/spinner.component';
import {MyProjectsComponent} from './pages/my-projects/my-projects.component';
import {PostService} from './service/post-service/post.service';
import {MyProjectDetilsComponent} from './pages/my-projects/my-project-details/my-project-details.component';
import {ProfileComponent} from './pages/profile/profile.component';
import {MyMessagesComponent} from './pages/my-messages/my-messages.component';
import {PostComponent} from './pages/news/posts.component';
import {AddPostModalComponent} from './pages/news/add-post-modal/add-post-modal.component';
import {AddNewMessagesComponent} from './pages/my-messages/add-new-messages/add-new-messages.component';
import {ChatService} from './service/chat-service/chat.service';
import {AddProjectComponent} from './pages/add-project/add-project.component';
import { FormWizardModule } from 'angular2-wizard';

const appRoutes: Routes = [
  {path: '', component: ProjectsComponent},
  {path: 'projects', component: ProjectsComponent},
  {path: 'posts', component: PostComponent},
  {path: 'about', component: AboutComponent},
  {path: 'my-profile', component: MyProfileComponent, canActivate: [RouteGuard]},
  {path: 'my-projects', component: MyProjectsComponent, canActivate: [RouteGuard]},
  {path: 'my-messages', component: MyMessagesComponent, canActivate: [RouteGuard]},
  {path: 'user-profile/:id', component: ProfileComponent},
  {path: 'my-project-details/:id', component: MyProjectDetilsComponent, canActivate: [RouteGuard]},
  {path: 'add-new-project', component: AddProjectComponent, canActivate: [RouteGuard]},
  {path: 'newMessage', component: AddNewMessagesComponent},
];

@NgModule({
  declarations: [
    AppComponent, AboutComponent, FooterComponent, HeaderComponent, SpinnerComponent, MyProjectsComponent, MyProjectDetilsComponent, ProfileComponent,
    PostComponent, ProjectsComponent, ProjectDetailsComponent, LoginComponent, RegisterComponent, MyProfileComponent, MyMessagesComponent, AddPostModalComponent,
    AddNewMessagesComponent, AddProjectComponent
  ],
  imports: [
    BrowserModule, FormsModule, ReactiveFormsModule, HttpModule,
    RouterModule.forRoot(appRoutes),
    CarouselModule.forRoot(),
    FormWizardModule
  ],
  providers: [ProjectService, UserService, AuthService, ReviewService, RouteGuard, PostService, ChatService],
  bootstrap: [AppComponent]
})
export class AppModule { }
