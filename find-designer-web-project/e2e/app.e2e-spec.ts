import { FindDesignerWebProjectPage } from './app.po';

describe('find-designer-web-project App', () => {
  let page: FindDesignerWebProjectPage;

  beforeEach(() => {
    page = new FindDesignerWebProjectPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
