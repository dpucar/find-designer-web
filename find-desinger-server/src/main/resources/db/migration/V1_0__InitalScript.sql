-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema design
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema design
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `design` DEFAULT CHARACTER SET cp1250 COLLATE cp1250_croatian_ci ;
USE `design` ;

-- -----------------------------------------------------
-- Table `design`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `design`.`user` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `surname` VARCHAR(45) NULL,
  `email` TEXT NULL,
  `password` VARCHAR(500) NULL,
  `user_picture_link` VARCHAR(1000) NULL,
  `date_created_profile` DATE NULL,
  `last_usage` DATE NULL,
  `enabled` TINYINT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `design`.`project_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `design`.`project_type` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `project_type_name` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `design`.`project`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `design`.`project` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `project_type` INT(11) NOT NULL,
  `name` VARCHAR(45) NULL,
  `description` TEXT NULL,
  `date_created` DATE NULL,
  `like_number` DOUBLE NULL,
  `review_number` DOUBLE NULL,
  `view_number` DOUBLE NULL,
  `enabled` TINYINT(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  INDEX `fk_work_work_type1_idx` (`project_type` ASC),
  CONSTRAINT `fk_work_work_type1`
    FOREIGN KEY (`project_type`)
    REFERENCES `design`.`project_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `design`.`user_project`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `design`.`user_project` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) NOT NULL,
  `project_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_user_work_user1_idx` (`user_id` ASC),
  INDEX `fk_user_work_work1_idx` (`project_id` ASC),
  CONSTRAINT `fk_user_work_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `design`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_work_work1`
    FOREIGN KEY (`project_id`)
    REFERENCES `design`.`project` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `design`.`project_pictures`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `design`.`project_pictures` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `link` TEXT NULL,
  `project_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_work_pictures_work1_idx` (`project_id` ASC),
  CONSTRAINT `fk_work_pictures_work1`
    FOREIGN KEY (`project_id`)
    REFERENCES `design`.`project` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `design`.`review`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `design`.`review` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `date_created` DATE NULL,
  `review` TEXT NULL,
  `user_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_review_user1_idx` (`user_id` ASC),
  CONSTRAINT `fk_review_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `design`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `design`.`project_reviews`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `design`.`project_reviews` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `project_id` INT(11) NOT NULL,
  `review_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_reviews_project1_idx` (`project_id` ASC),
  INDEX `fk_reviews_review1_idx` (`review_id` ASC),
  CONSTRAINT `fk_reviews_project1`
    FOREIGN KEY (`project_id`)
    REFERENCES `design`.`project` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_reviews_review1`
    FOREIGN KEY (`review_id`)
    REFERENCES `design`.`review` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `design`.`skills`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `design`.`skills` (
  `id` INT(11) NOT NULL,
  `user_id` INT(11) NOT NULL,
  `name` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_skills_user1_idx` (`user_id` ASC),
  CONSTRAINT `fk_skills_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `design`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `design`.`post`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `design`.`post` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NULL,
  `text` TEXT NULL,
  `date_created` TIMESTAMP NULL,
  `project_id` INT(11) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_post_project1_idx` (`project_id` ASC),
  CONSTRAINT `fk_post_project1`
    FOREIGN KEY (`project_id`)
    REFERENCES `design`.`project` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `design`.`user_posts`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `design`.`user_posts` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) NOT NULL,
  `post_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_user_post_post1_idx` (`post_id` ASC),
  INDEX `fk_user_post_user1_idx` (`user_id` ASC),
  CONSTRAINT `fk_user_post_post1`
    FOREIGN KEY (`post_id`)
    REFERENCES `design`.`post` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_post_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `design`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `design`.`project_likes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `design`.`project_likes` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `project_id` INT(11) NOT NULL,
  `user_id` INT(11) NOT NULL,
  `is_liked` TINYINT(1) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_project_likes_project1_idx` (`project_id` ASC),
  INDEX `fk_project_likes_user1_idx` (`user_id` ASC),
  CONSTRAINT `fk_project_likes_project1`
    FOREIGN KEY (`project_id`)
    REFERENCES `design`.`project` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_project_likes_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `design`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `design`.`roles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `design`.`roles` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `design`.`user_roles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `design`.`user_roles` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) NOT NULL,
  `role_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_user_roles_user1_idx` (`user_id` ASC),
  INDEX `fk_user_roles_roles1_idx` (`role_id` ASC),
  CONSTRAINT `fk_user_roles_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `design`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_roles_roles1`
    FOREIGN KEY (`role_id`)
    REFERENCES `design`.`roles` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `design`.`messages`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `design`.`messages` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `sender_id` INT(11) NOT NULL,
  `reciever_id` INT(11) NOT NULL,
  `chatroom` VARCHAR(255) NOT NULL,
  `chatroom_title` VARCHAR(255) NOT NULL,
  `created_at` TIMESTAMP NOT NULL,
  `message` TEXT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_messages_user1_idx` (`sender_id` ASC),
  INDEX `fk_messages_user2_idx` (`reciever_id` ASC),
  CONSTRAINT `fk_messages_user1`
  FOREIGN KEY (`sender_id`)
  REFERENCES `design`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_messages_user2`
  FOREIGN KEY (`reciever_id`)
  REFERENCES `design`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `design`.`newsletter`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `design`.`newsletter` (
  `email` VARCHAR(500) NOT NULL,
  PRIMARY KEY (`email`))
  ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
