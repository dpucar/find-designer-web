INSERT INTO `roles`(`name`) VALUES ('USER');
INSERT INTO `roles`(`name`) VALUES ('DESIGNER');
INSERT INTO `roles`(`name`) VALUES ('ADMIN');
INSERT INTO `roles`(`name`) VALUES ('RESETPASS');


INSERT INTO `project_type`(`id`, `project_type_name`) VALUES(1, 'Web application');
INSERT INTO `project_type`(`id`, `project_type_name`) VALUES(2, 'Mobile application');

INSERT INTO `user`(`name`, `surname`, `email`, `password`, `user_picture_link`, `enabled`)
VALUES('Designer', 'Designer', 'designer@mail.com', '$2a$06$iUcUK3tNXSVP.OOxbiFeIe2rCBrksaHXHorgdWiHQckmSgZ2yQjGC', 'http://www.bruisedpassports.com/wordpress/wp-content/uploads/2015/03/Kakslauttanen-Arctic-Resort-Lapland-Finland-14-1024x684.jpg', 1);

INSERT INTO `user`(`name`, `surname`, `email`, `password`, `user_picture_link`, `enabled`)
	VALUES('Ivan', 'Ivanov', 'ivan@mail.com', '$2a$06$iUcUK3tNXSVP.OOxbiFeIe2rCBrksaHXHorgdWiHQckmSgZ2yQjGC', 'http://www.bruisedpassports.com/wordpress/wp-content/uploads/2015/03/Kakslauttanen-Arctic-Resort-Lapland-Finland-14-1024x684.jpg', 1);

INSERT INTO `user`(`name`, `surname`, `email`, `password`, `user_picture_link`, `enabled`)
	VALUES('Marko', 'Simov', 'marko@mail.com', '$2a$06$iUcUK3tNXSVP.OOxbiFeIe2rCBrksaHXHorgdWiHQckmSgZ2yQjGC', 'https://www.alvinailey.org/sites/default/files/styles/slideshow_image/public/melanie-person.jpg?itok=ocw3xkx_', 1);

INSERT INTO `user`(`name`, `surname`, `email`, `password`, `user_picture_link`, `enabled`)
VALUES('John', 'Smith', 'john@mail.com', '$2a$06$iUcUK3tNXSVP.OOxbiFeIe2rCBrksaHXHorgdWiHQckmSgZ2yQjGC','http://static-23.sinclairstoryline.com/resources/media/d368a1cb-f6af-4a04-83d0-ebc66e984ab2-large16x9_BIOPIC.jpg', 1);

INSERT INTO `user`(`name`, `surname`, `email`, `password`, `user_picture_link`, `enabled`)
VALUES('Sony', 'Bony', 'sony@mail.com', '$2a$06$iUcUK3tNXSVP.OOxbiFeIe2rCBrksaHXHorgdWiHQckmSgZ2yQjGC','http://www.adiguzel.edu.tr/wp-content/uploads/2013/05/team5.jpg', 1);

INSERT INTO `user`(`name`, `surname`, `email`, `password`, `user_picture_link`, `enabled`)
VALUES('Patrick', 'Nicer', 'pat@mail.com', '$2a$06$iUcUK3tNXSVP.OOxbiFeIe2rCBrksaHXHorgdWiHQckmSgZ2yQjGC','https://upload.wikimedia.org/wikipedia/commons/a/a0/Andrzej_Person_Kancelaria_Senatu.jpg', 1);

INSERT INTO `user`(`name`, `surname`, `email`, `password`, `user_picture_link`, `enabled`)
VALUES('Josip', 'Zagorac', 'jole@mail.com', '$2a$06$iUcUK3tNXSVP.OOxbiFeIe2rCBrksaHXHorgdWiHQckmSgZ2yQjGC','http://3fx68p2c0lxxl33uqe6mc216p5.wpengine.netdna-cdn.com/wp-content/uploads/2013/12/steve-wozniak.jpg', 1);

INSERT INTO `user`(`name`, `surname`, `email`, `password`, `user_picture_link`, `enabled`)
VALUES('Joan', 'Miro', 'joan@mail.com', '$2a$06$iUcUK3tNXSVP.OOxbiFeIe2rCBrksaHXHorgdWiHQckmSgZ2yQjGC','https://uploads4.wikiart.org/images/joan-miro/the-smile-of-the-flamboyant-wings.jpg', 1);

INSERT INTO `project`(`id`, `project_type`, `name`, `description`, `date_created`, `like_number`, `review_number`, `view_number`)
VALUES(1, 1, 'Design for Historic.com', 'Something very strange and odd!', '2016-5-5', 21, 5, 1000);
INSERT INTO `project`(`id`, `project_type`, `name`, `description`, `date_created`, `like_number`, `review_number`, `view_number`)
VALUES(2, 2, 'Design for ABC.com', 'My mission was to accomplish their journey to the center of....', '2016-5-5', 21, 2, 564);
INSERT INTO `project`(`id`, `project_type`, `name`, `description`, `date_created`, `like_number`, `review_number`, `view_number`)
VALUES(3, 1, 'Example project', 'Example project designed for schoole in Africa', '2016-5-5', 21, 1, 23);
INSERT INTO `project`(`id`, `project_type`, `name`, `description`, `date_created`, `like_number`, `review_number`, `view_number`)
VALUES(4, 1, 'Project four', 'Example project four', '2016-5-5', 21, 1, 23);

INSERT INTO `project`(`id`, `project_type`, `name`, `description`, `date_created`, `like_number`, `review_number`, `view_number`)
VALUES(5, 1, 'Project five', 'Example project five', '2016-5-5', 21, 1, 23);
INSERT INTO `project`(`id`, `project_type`, `name`, `description`, `date_created`, `like_number`, `review_number`, `view_number`)
VALUES(6, 1, 'Project six', 'Example project six', '2016-5-5', 21, 1, 23);
INSERT INTO `project`(`id`, `project_type`, `name`, `description`, `date_created`, `like_number`, `review_number`, `view_number`)
VALUES(7, 1, 'Project seven', 'Example project seven', '2016-5-5', 21, 1, 23);
INSERT INTO `project`(`id`, `project_type`, `name`, `description`, `date_created`, `like_number`, `review_number`, `view_number`)
VALUES(8, 1, 'Project eight', 'Example project eight', '2016-5-5', 21, 1, 23);
INSERT INTO `project`(`id`, `project_type`, `name`, `description`, `date_created`, `like_number`, `review_number`, `view_number`)
VALUES(9, 1, 'Project nine', 'Example project nine', '2016-5-5', 21, 1, 23);
INSERT INTO `project`(`id`, `project_type`, `name`, `description`, `date_created`, `like_number`, `review_number`, `view_number`)
VALUES(10, 1, 'Project ten', 'Example project ten', '2016-5-5', 21, 1, 23);
INSERT INTO `project`(`id`, `project_type`, `name`, `description`, `date_created`, `like_number`, `review_number`, `view_number`)
VALUES(11, 1, 'Project eleven', 'Example project eleven', '2016-5-5', 21, 1, 23);
INSERT INTO `project`(`id`, `project_type`, `name`, `description`, `date_created`, `like_number`, `review_number`, `view_number`)
VALUES(12, 1, 'Project twelve', 'Example project twelve', '2016-5-5', 21, 1, 23);
INSERT INTO `project`(`id`, `project_type`, `name`, `description`, `date_created`, `like_number`, `review_number`, `view_number`)
VALUES(13, 1, 'Project thirteen', 'Example project thirteen', '2016-5-5', 21, 1, 23);
INSERT INTO `project`(`id`, `project_type`, `name`, `description`, `date_created`, `like_number`, `review_number`, `view_number`)
VALUES(14, 1, 'Project fourteen', 'Example project fourteen', '2016-5-5', 21, 1, 23);


INSERT INTO `user_project`(`id`, `user_id`, `project_id`) VALUES(1, 2, 1);
INSERT INTO `user_project`(`id`, `user_id`, `project_id`) VALUES(2, 3, 2);
INSERT INTO `user_project`(`id`, `user_id`, `project_id`) VALUES(3, 4, 3);
INSERT INTO `user_project`(`id`, `user_id`, `project_id`) VALUES(4, 5, 4);
INSERT INTO `user_project`(`id`, `user_id`, `project_id`) VALUES(5, 6, 5);
INSERT INTO `user_project`(`id`, `user_id`, `project_id`) VALUES(6, 7, 6);
INSERT INTO `user_project`(`id`, `user_id`, `project_id`) VALUES(7, 7, 7);
INSERT INTO `user_project`(`id`, `user_id`, `project_id`) VALUES(8, 8, 8);
INSERT INTO `user_project`(`id`, `user_id`, `project_id`) VALUES(9, 5, 9);
INSERT INTO `user_project`(`id`, `user_id`, `project_id`) VALUES(10, 4, 10);


INSERT INTO `project_pictures`(`link`, `project_id`) VALUES ('http://www.planwallpaper.com/static/images/ComputerDesktopWallpapersCollection540_047_inxQEjv.jpg', 1);
INSERT INTO `project_pictures`(`link`, `project_id`) VALUES ('http://geodavephotography.com/images/design-wallpapers/38190605-design-wallpapers.jpg', 1);
INSERT INTO `project_pictures`(`link`, `project_id`) VALUES ('http://weandthecolor.com/wp-content/uploads/2013/01/A-Sunny-day-on-a-Cloudy-day-Design-Project-546335.jpg', 2);
INSERT INTO `project_pictures`(`link`, `project_id`) VALUES ('http://www.coreldraw.com/static/cdgs/product_content/cdgs/2017/pillar-01.jpg', 2);
INSERT INTO `project_pictures`(`link`, `project_id`) VALUES ('http://abduzeedo.com/sites/default/files/styles/home_cover/public/originals/exotic-destinations-illustration.jpg?itok=o8jsp-QO', 3);
INSERT INTO `project_pictures`(`link`, `project_id`) VALUES ('https://daks2k3a4ib2z.cloudfront.net/583347ca8f6c7ee058111b55/590106989c5b1a42e9bf9d6c_best-website-gallery.png', 3);
INSERT INTO `project_pictures`(`link`, `project_id`) VALUES ('http://www.designyourway.net/diverse/5/twentyfive/music_app.jpg', 4);
INSERT INTO `project_pictures`(`link`, `project_id`) VALUES ('http://www.julessdesign.com/wp-content/uploads/2015/10/ui-design.jpg', 4);
INSERT INTO `project_pictures`(`link`, `project_id`) VALUES ('http://www.planwallpaper.com/static/images/ComputerDesktopWallpapersCollection540_047_inxQEjv.jpg', 5);
INSERT INTO `project_pictures`(`link`, `project_id`) VALUES ('https://file.mockplus.com/image/2017/05/04073f83-6ebf-4e20-ad8c-0c3057169b14.jpg', 5);
INSERT INTO `project_pictures`(`link`, `project_id`) VALUES ('http://tubikstudio.com/wp-content/uploads/2015/12/tubik-studio-ui-design.jpg', 6);
INSERT INTO `project_pictures`(`link`, `project_id`) VALUES ('http://www.planwallpaper.com/static/images/ComputerDesktopWallpapersCollection540_047_inxQEjv.jpg', 6);
INSERT INTO `project_pictures`(`link`, `project_id`) VALUES ('http://www.topdesignmag.com/wp-content/uploads/2013/03/3.-user-interface-design.jpg', 7);
INSERT INTO `project_pictures`(`link`, `project_id`) VALUES ('http://www.designyourway.net/diverse/3/uidashboards/1400070.jpg', 7);
INSERT INTO `project_pictures`(`link`, `project_id`) VALUES ('http://www.graphberry.com/images/img/98/730.jpg', 8);
INSERT INTO `project_pictures`(`link`, `project_id`) VALUES ('http://www.webdesign-inspiration.com/uploads/design/2015-02/tm1.jpg', 9);
INSERT INTO `project_pictures`(`link`, `project_id`) VALUES ('http://www.topdesignmag.com/wp-content/uploads/2013/03/3.-user-interface-design.jpg', 9);
INSERT INTO `project_pictures`(`link`, `project_id`) VALUES ('https://file.mockplus.com/image/2017/05/04073f83-6ebf-4e20-ad8c-0c3057169b14.jpg', 10);
INSERT INTO `project_pictures`(`link`, `project_id`) VALUES ('https://s-media-cache-ak0.pinimg.com/originals/6a/c1/6c/6ac16cb1ff62ce6badc9f27864e57272.jpg', 10);

INSERT INTO review(`id`, `date_created`, `review`, `user_id`) VALUES(1, '2015-05-05', 'Najbolji dizajn koji sam ikad vidio!', 2);
INSERT INTO review(`id`, `date_created`, `review`, `user_id`) VALUES(2, '2015-05-05', 'Izvrstan dizajn!', 3);
INSERT INTO review(`id`, `date_created`, `review`, `user_id`) VALUES(3, '2015-05-05', 'Sjajno sviđa mi se!', 4);

INSERT INTO project_reviews(`id`, `project_id`, `review_id`) VALUES (1, 1, 1);
INSERT INTO project_reviews(`id`, `project_id`, `review_id`) VALUES (2, 1, 2);
INSERT INTO project_reviews(`id`, `project_id`, `review_id`) VALUES (3, 1, 3);

INSERT INTO post(`id`, `title`, `text`, `date_created`, `project_id`) VALUES (1, 'AppleStoreMountainDesign', 'Pogledajte moj novi dizajn pod nazivom Apple WebStore, komentirajte, lajkajte!', '2016-04-04', 1);
INSERT INTO post(`id`, `title`, `text`, `date_created`, `project_id`) VALUES (2, 'What color?!', 'Razmišljam je li bolja crvena boja ili plava?! Trebam pomoć!', '2016-04-04', 2);
INSERT INTO post(`id`, `title`, `text`, `date_created`, `project_id`) VALUES (3, 'Programski alati', 'Programski alati u programiranju i njihova primjena!', '2016-04-04', 3);
INSERT INTO post(`id`, `title`, `text`, `date_created`, `project_id`) VALUES (4, 'UI dizajn za tvrku "Početak 02"', 'UI dizajn temeljen na detaljnom istraživanju korisnika tvrkte "Početak 02"', '2016-04-04', 4);
INSERT INTO post(`id`, `title`, `text`, `date_created`, `project_id`) VALUES (5, 'Korisničko sučelje nove generacije', 'Izradio sam korisničko sučelje koje će sigurno osvojiti prvo mjesto na natjecanju!', '2016-04-04', 5);
INSERT INTO post(`id`, `title`, `text`, `date_created`, `project_id`) VALUES (6, 'Mobilna aplikacija Limitess', 'Kako dizajnirati aplikaciju s bezbroj mogućnosti?', '2016-04-04', 6);
INSERT INTO post(`id`, `title`, `text`, `date_created`, `project_id`) VALUES (7, 'Provjera dizajna izrade', 'Provjera dizajna prije upotrebe?', '2016-04-04', 7);
INSERT INTO post(`id`, `title`, `text`, `date_created`, `project_id`) VALUES (8, 'Super Red Alert Design', 'Dizajn opasnih namjera! Provjerite i ocjenite!', '2016-04-04', 8);


INSERT INTO user_posts(`id`, `user_id`, `post_id`) VALUES(1, 2, 1);
INSERT INTO user_posts(`id`, `user_id`, `post_id`) VALUES(2, 3, 2);
INSERT INTO user_posts(`id`, `user_id`, `post_id`) VALUES(3, 4, 3);
INSERT INTO user_posts(`id`, `user_id`, `post_id`) VALUES(4, 5, 4);
INSERT INTO user_posts(`id`, `user_id`, `post_id`) VALUES(5, 6, 5);
INSERT INTO user_posts(`id`, `user_id`, `post_id`) VALUES(6, 7, 6);
INSERT INTO user_posts(`id`, `user_id`, `post_id`) VALUES(7, 8, 7);
INSERT INTO user_posts(`id`, `user_id`, `post_id`) VALUES(8, 8, 8);

INSERT INTO `user_roles`(`user_id`, `role_id`) VALUES (1, 1);
INSERT INTO `user_roles`(`user_id`, `role_id`) VALUES (2, 1);
INSERT INTO `user_roles`(`user_id`, `role_id`) VALUES (3, 2);
INSERT INTO `user_roles`(`user_id`, `role_id`) VALUES (4, 2);
INSERT INTO `user_roles`(`user_id`, `role_id`) VALUES (5, 2);
INSERT INTO `user_roles`(`user_id`, `role_id`) VALUES (6, 2);
INSERT INTO `user_roles`(`user_id`, `role_id`) VALUES (7, 2);
INSERT INTO `user_roles`(`user_id`, `role_id`) VALUES (8, 2);
