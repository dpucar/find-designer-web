package hr.finddesigner.repository;

import hr.finddesigner.model.entity.ProjectPictures;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProjectPicturesRepository extends JpaRepository<ProjectPictures, Long> {
    List<ProjectPictures> findAllByProject_Id(Long id);
}
