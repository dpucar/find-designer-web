package hr.finddesigner.repository;

import hr.finddesigner.model.entity.ProjectReview;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectReviewRepository extends JpaRepository<ProjectReview, Long> {
    Iterable<ProjectReview> findAllByProject_Id(Long id);
}
