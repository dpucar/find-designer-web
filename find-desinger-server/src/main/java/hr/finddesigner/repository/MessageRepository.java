package hr.finddesigner.repository;

import hr.finddesigner.model.entity.Message;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MessageRepository extends JpaRepository<Message, Long> {

    Iterable<Message> findByChatroomOrderByCreatedAtAsc(String chatroom);

    List<Message> findAllBySender_Id(Long id);

    List<Message> findAllByReciever_Id(Long id);
}
