package hr.finddesigner.repository;

import hr.finddesigner.model.entity.ProjectType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectTypeRepository extends JpaRepository<ProjectType, Long>{
}
