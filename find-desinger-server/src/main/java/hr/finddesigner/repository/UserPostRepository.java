package hr.finddesigner.repository;

import hr.finddesigner.model.entity.UserPosts;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserPostRepository extends JpaRepository<UserPosts, Long> {
}
