package hr.finddesigner.repository;

import hr.finddesigner.model.entity.UserProject;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserProjectRepository extends JpaRepository<UserProject, Long> {
    Iterable<UserProject> findByUser_Id(Long id);

    UserProject findUserProjectByProject_Id(Long id);

    Iterable<UserProject> findAllByUser_Id(Long id);
}
