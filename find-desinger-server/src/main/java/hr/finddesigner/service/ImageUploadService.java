package hr.finddesigner.service;

public interface ImageUploadService {
    String save(byte[] imageData, String fileName);
}
