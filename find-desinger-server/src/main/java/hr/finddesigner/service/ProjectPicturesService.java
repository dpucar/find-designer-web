package hr.finddesigner.service;

import hr.finddesigner.model.entity.ProjectPictures;

import java.util.List;

public interface ProjectPicturesService {
    ProjectPictures saveProjectPicture(ProjectPictures projectPictures);

    List<ProjectPictures> saveProjectPictures(Iterable<ProjectPictures> projectPictures);
}
