package hr.finddesigner.service;

import hr.finddesigner.model.entity.Project;


public interface ProjectService {
    Project getProjectById(Long id);

    Iterable<Project> getProjects();

    Project saveProject(Project project);

    Project updateProject(Long id, Project project);
}
