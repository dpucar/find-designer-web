package hr.finddesigner.service;

import hr.finddesigner.model.entity.Post;
import hr.finddesigner.model.entity.UserPosts;

public interface PostService {
    Iterable<Post> findAllPosts();

    Iterable<UserPosts> findAllUserPosts();

    Post savePost(Post post);

    UserPosts saveUserPost(UserPosts userPost);
}
