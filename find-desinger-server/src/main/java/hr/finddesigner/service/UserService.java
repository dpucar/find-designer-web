package hr.finddesigner.service;

import hr.finddesigner.model.entity.User;

public interface UserService {
    User getUserById(Long id);

    User findByEmail(String email);

    Iterable<User> getAllUsers();

    User registerUserAndRole(User user, String role);

    Boolean doesUserEmailExists(String email);

    User updateUser(User user);
}
