package hr.finddesigner.service;


import hr.finddesigner.model.entity.Project;
import hr.finddesigner.model.entity.UserProject;

public interface UserProjectService {
    Iterable<Project> getProjectsByUserId(Long id);

    UserProject saveUserProject(UserProject userProject);

    Iterable<UserProject> findAllUserProjects();

    UserProject findUserProjectByProjectId(Long id);

    Iterable<UserProject> findAllByUserId(Long id);
}
