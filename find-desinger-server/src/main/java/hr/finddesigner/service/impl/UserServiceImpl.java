package hr.finddesigner.service.impl;

import hr.finddesigner.model.entity.Role;
import hr.finddesigner.model.entity.User;
import hr.finddesigner.model.entity.UserRole;
import hr.finddesigner.repository.RoleRepository;
import hr.finddesigner.repository.UserRepository;
import hr.finddesigner.repository.UserRoleRepository;
import hr.finddesigner.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final RoleRepository roleRepository;
    private final UserRoleRepository userRoleRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder, RoleRepository roleRepository, UserRoleRepository userRoleRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.roleRepository = roleRepository;
        this.userRoleRepository = userRoleRepository;
    }

    @Override
    public User getUserById(Long id) {
        return userRepository.findOne(id);
    }

    @Override
    public Iterable<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public User updateUser(User user) {
        User oldUser = userRepository.findOne(user.getId());

        if(!oldUser.getPassword().equals(user.getPassword())){
            user.setPassword(passwordEncoder.encode(user.getPassword()));
        }

        return userRepository.save(user);
    }

    @Override
    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public User registerUserAndRole(User user, String roleName){
        user.setPassword(passwordEncoder.encode(user.getPassword()));

        User u = userRepository.save(user);
        userRoleRepository.save(createUserRole(u, roleName));

        return u;
    }

    @Override
    public Boolean doesUserEmailExists(String email) {
        if(userRepository.findByEmail(email) != null){
            return true;
        }
        return false;
    }

    private UserRole createUserRole(User user, String roleName) {
        Role role = roleRepository.findByName(roleName);

        UserRole userRole = new UserRole();
        userRole.setUser(user);
        userRole.setRole(role);

        return userRole;
    }
}
