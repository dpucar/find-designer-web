package hr.finddesigner.service.impl;

import hr.finddesigner.model.entity.ProjectType;
import hr.finddesigner.repository.ProjectTypeRepository;
import hr.finddesigner.service.ProjectTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProjectTypeServiceImpl implements ProjectTypeService{
    private final ProjectTypeRepository projectTypeRepository;

    @Autowired
    public ProjectTypeServiceImpl(ProjectTypeRepository projectTypeRepository) {
        this.projectTypeRepository = projectTypeRepository;
    }

    @Override
    public List<ProjectType> getAllProjectTypes() {
        return projectTypeRepository.findAll();
    }
}
