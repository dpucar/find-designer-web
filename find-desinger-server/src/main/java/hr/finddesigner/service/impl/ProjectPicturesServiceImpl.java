package hr.finddesigner.service.impl;

import hr.finddesigner.model.entity.ProjectPictures;
import hr.finddesigner.repository.ProjectPicturesRepository;
import hr.finddesigner.service.ProjectPicturesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProjectPicturesServiceImpl implements ProjectPicturesService{
    private final ProjectPicturesRepository projectPicturesRepository;

    @Autowired
    public ProjectPicturesServiceImpl(ProjectPicturesRepository projectPicturesRepository) {
        this.projectPicturesRepository = projectPicturesRepository;
    }

    @Override
    public ProjectPictures saveProjectPicture(ProjectPictures projectPictures) {
        return projectPicturesRepository.save(projectPictures);
    }

    @Override
    public List<ProjectPictures> saveProjectPictures(Iterable<ProjectPictures> projectPictures) {
        return projectPicturesRepository.save(projectPictures);
    }
}
