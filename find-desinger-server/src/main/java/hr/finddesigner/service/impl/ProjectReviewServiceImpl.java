package hr.finddesigner.service.impl;

import hr.finddesigner.model.entity.ProjectReview;
import hr.finddesigner.model.entity.Review;
import hr.finddesigner.repository.ProjectReviewRepository;
import hr.finddesigner.service.ProjectReviewService;
import hr.finddesigner.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProjectReviewServiceImpl implements ProjectReviewService{
    public final ProjectReviewRepository projectReviewRepository;
    public final UserService userService;


    @Autowired
    public ProjectReviewServiceImpl(ProjectReviewRepository projectReviewRepository, UserService userService) {
        this.projectReviewRepository = projectReviewRepository;
        this.userService = userService;
    }

    @Override
    public ProjectReview saveProjectReview(ProjectReview projectReview) {
        return projectReviewRepository.save(projectReview);
    }

    @Override
    public List<Review> findByProjectId(Long id){
        List<Review> reviews = new ArrayList<>();

        Iterable<ProjectReview> projectReviews =  projectReviewRepository.findAllByProject_Id(id);
        projectReviews.forEach(pr -> reviews.add(pr.getReview()));

        return reviews;
    }
}
