package hr.finddesigner.service.impl;

import hr.finddesigner.security.SecurityConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

@Service
public class UserAuthService {

    @Autowired
    private SecurityConfig securityConfig;

    public Authentication authenticate(final String username, final String password) throws Exception {
        return securityConfig.authenticationManagerBean()
                .authenticate(new UsernamePasswordAuthenticationToken(username, password));
    }
}
