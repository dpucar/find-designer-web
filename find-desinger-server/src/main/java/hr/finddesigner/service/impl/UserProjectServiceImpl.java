package hr.finddesigner.service.impl;

import hr.finddesigner.model.entity.Project;
import hr.finddesigner.model.entity.UserProject;
import hr.finddesigner.repository.UserProjectRepository;
import hr.finddesigner.service.UserProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserProjectServiceImpl implements UserProjectService {
    private final UserProjectRepository userProjectRepository;

    @Autowired
    public UserProjectServiceImpl(UserProjectRepository userProjectRepository) {
        this.userProjectRepository = userProjectRepository;
    }

    @Override
    public Iterable<Project> getProjectsByUserId(Long id) {
        List<Project> projects = new ArrayList<>();
        Iterable<UserProject> userProjects = userProjectRepository.findByUser_Id(id);

        userProjects.forEach(up -> projects.add(up.getProject()));

        return projects;
    }

    @Override
    public UserProject saveUserProject(UserProject userProject) {
        return userProjectRepository.save(userProject);
    }

    @Override
    public Iterable<UserProject> findAllUserProjects() {
        return userProjectRepository.findAll();
    }

    @Override
    public UserProject findUserProjectByProjectId(Long id) {
        return userProjectRepository.findUserProjectByProject_Id(id);
    }

    @Override
    public Iterable<UserProject> findAllByUserId(Long id) {
        return userProjectRepository.findAllByUser_Id(id);
    }
}
