package hr.finddesigner.service.impl;

import hr.finddesigner.model.entity.Project;
import hr.finddesigner.model.entity.ProjectPictures;
import hr.finddesigner.repository.ProjectPicturesRepository;
import hr.finddesigner.repository.ProjectRepository;
import hr.finddesigner.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProjectServiceImpl implements ProjectService {
    private final ProjectRepository projectRepository;
    private final ProjectPicturesRepository projectPicturesRepository;

    @Autowired
    public ProjectServiceImpl(ProjectRepository projectRepository, ProjectPicturesRepository projectPicturesRepository) {
        this.projectRepository = projectRepository;
        this.projectPicturesRepository = projectPicturesRepository;
    }

    @Override
    public Project getProjectById(Long id) {
        return projectRepository.findOne(id);
    }

    @Override
    public Iterable<Project> getProjects() {
        return projectRepository.findAll();
    }

    @Override
    public Project saveProject(Project project) {
        return projectRepository.save(project);
    }

    @Override
    public Project updateProject(Long id, Project project) {
        List<ProjectPictures> oldPictures = projectPicturesRepository.findAllByProject_Id(id);

        for(ProjectPictures pOld : oldPictures){
            boolean exists = false;
            for(ProjectPictures pNew : project.getProjectPictures()){
                if(pOld.getId() == pNew.getId()){
                    exists = true;
                }
            }

            if(!exists){
                projectPicturesRepository.delete(pOld.getId());
            }
        }

        return projectRepository.save(project);
    }
}
