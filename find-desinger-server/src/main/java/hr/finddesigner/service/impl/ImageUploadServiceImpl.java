package hr.finddesigner.service.impl;

import com.cloudinary.Cloudinary;
import hr.finddesigner.service.ImageUploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;

@Service
public class ImageUploadServiceImpl implements ImageUploadService{
    private static final String URL = "url";

    private final Cloudinary cloudinary;

    @Autowired
    public ImageUploadServiceImpl(Cloudinary cloudinary) {
        this.cloudinary = cloudinary;
    }

    @Override
    public String save(byte[] imageData, String fileName) {
        try {
            Map uploadResult = cloudinary.uploader().upload(imageData, Collections.emptyMap());
            return (String) uploadResult.get(URL);
        } catch (IOException e) {
            throw new ImageStorageException(e);
        }
    }

    private class ImageStorageException extends RuntimeException {
        ImageStorageException(Throwable throwable) {
            super(throwable);
        }
    }
}