package hr.finddesigner.service.impl;

import hr.finddesigner.model.dto.DisplayMessageList;
import hr.finddesigner.model.entity.Message;
import hr.finddesigner.repository.MessageRepository;
import hr.finddesigner.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MessageServiceImpl implements MessageService {
    private final MessageRepository messageRepository;


    @Autowired
    public MessageServiceImpl(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }


    @Override
    public Iterable<Message> findAll(){
        return messageRepository.findAll();
    }

    @Override
    public Message findOne(Long id){
        return messageRepository.findOne(id);
    }

    @Override
    public Iterable<Message> findByChatroomOrderByCreatedAtAsc(String chatroom){
        return messageRepository.findByChatroomOrderByCreatedAtAsc(chatroom);
    }

    @Override
    public Message saveChat(Message message){
        return messageRepository.save(message);
    }

    @Override
    public Message checkIfChatroomExists(Long senderId, Long recieverId) {
        boolean exists = false;
        Message message = null;

        List<Message> senderMessages = messageRepository.findAllBySender_Id(senderId);
        List<Message> recieverMessages = messageRepository.findAllBySender_Id(recieverId);

        for(Message m1 : senderMessages){
            for(Message m2 : recieverMessages){
                if(m1.getChatroom().equals(m2.getChatroom())){
                    exists = true;
                    message = m1;
                    break;
                }
            }
        }

        if(!exists){
            return new Message();
        }

        return message;
    }

    @Override
    public List<DisplayMessageList> getMessagesForUser(Long userId) {
        List<DisplayMessageList> displayMessageLists = new ArrayList<>();
        List<String> chatrooms = new ArrayList<>();
        List<Message> senderMessages = messageRepository.findAllBySender_Id(userId);
        List<Message> recieverMessages = messageRepository.findAllByReciever_Id(userId);

        for(Message m1 : senderMessages){
            //if exists do nothing
            //if not exists add to displaymessagelist and on chatrooms
            if(!checkIfChatroomIsOnTheList(chatrooms, m1.getChatroom())){
                DisplayMessageList displayMessageList = new DisplayMessageList(m1.getChatroom(), m1.getChatroomTitle());
                displayMessageLists.add(displayMessageList);
                chatrooms.add(m1.getChatroom());
            }
        }

        for(Message m2 : recieverMessages){
            //if exists do nothing
            //if not exists add to displaymessagelist and on chatrooms
            if(!checkIfChatroomIsOnTheList(chatrooms, m2.getChatroom())){
                DisplayMessageList displayMessageList = new DisplayMessageList(m2.getChatroom(), m2.getChatroomTitle());
                displayMessageLists.add(displayMessageList);
                chatrooms.add(m2.getChatroom());
            }
        }

        return displayMessageLists;
    }

    private boolean checkIfChatroomIsOnTheList(List<String> chatrooms, String chatroom) {
        boolean exists = false;

        for(String ch : chatrooms){
            if(ch.equals(chatroom)){
                exists = true;
                break;
            }
        }

        return exists;
    }
}
