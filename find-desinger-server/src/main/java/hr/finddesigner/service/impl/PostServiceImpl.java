package hr.finddesigner.service.impl;

import hr.finddesigner.model.entity.Post;
import hr.finddesigner.model.entity.UserPosts;
import hr.finddesigner.repository.PostRepository;
import hr.finddesigner.repository.UserPostRepository;
import hr.finddesigner.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PostServiceImpl implements PostService {
    private final PostRepository postRepository;
    private final UserPostRepository userPostRepository;

    @Autowired
    public PostServiceImpl(PostRepository postRepository, UserPostRepository userPostRepository) {
        this.postRepository = postRepository;
        this.userPostRepository = userPostRepository;
    }

    @Override
    public Iterable<Post> findAllPosts() {
        return postRepository.findAll();
    }

    @Override
    public Iterable<UserPosts> findAllUserPosts() {
        return userPostRepository.findAll();
    }

    @Override
    public Post savePost(Post post) {
        return postRepository.save(post);
    }

    @Override
    public UserPosts saveUserPost(UserPosts userPost) {
        return userPostRepository.save(userPost);
    }
}
