package hr.finddesigner.service;

import hr.finddesigner.model.dto.DisplayMessageList;
import hr.finddesigner.model.entity.Message;

import java.util.List;

public interface MessageService {
    Iterable<Message> findAll();

    Message findOne(Long id);

    Iterable<Message> findByChatroomOrderByCreatedAtAsc(String chatroom);

    Message saveChat(Message message);

    Message checkIfChatroomExists(Long senderId, Long recieverId);

    List<DisplayMessageList> getMessagesForUser(Long senderId);
}