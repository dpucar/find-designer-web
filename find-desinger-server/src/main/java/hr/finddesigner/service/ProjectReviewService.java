package hr.finddesigner.service;

import hr.finddesigner.model.entity.ProjectReview;
import hr.finddesigner.model.entity.Review;

import java.util.List;

public interface ProjectReviewService {
    ProjectReview saveProjectReview(ProjectReview projectReview);

    List<Review> findByProjectId(Long id);
}
