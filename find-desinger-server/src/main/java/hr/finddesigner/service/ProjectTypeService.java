package hr.finddesigner.service;

import hr.finddesigner.model.entity.ProjectType;

import java.util.List;

public interface ProjectTypeService {
    List<ProjectType> getAllProjectTypes();
}
