package hr.finddesigner.service;

import hr.finddesigner.model.entity.Review;

public interface ReviewService {
    Iterable<Review> findAll();

    Review saveReview(Review review);
}
