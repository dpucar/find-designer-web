package hr.finddesigner.model.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO {

    private Long id;

    private String name;

    private String surname;

    private String email;

    private String password;

    private String userPicture;
    private Date dateCreated;

    private Date lastUsage;


    private boolean enabled;
}
