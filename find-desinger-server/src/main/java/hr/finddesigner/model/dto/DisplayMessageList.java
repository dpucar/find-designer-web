package hr.finddesigner.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DisplayMessageList {
    private String chatroomId;
    private String chatroomName;
}
