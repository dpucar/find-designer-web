package hr.finddesigner.model.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


@Entity
@Data
@NoArgsConstructor
public class Project implements Serializable{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    private String description;

    @Column(name = "date_created")
    private Date dateCreated;

    @Column(name = "like_number")
    private Double likeNumber;

    @Column(name = "review_number")
    private Double reviewNumber;

    @Column(name = "view_number")
    private Double viewNumber;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "project_reviews", joinColumns = @JoinColumn(name = "project_id",
            referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "review_id", referencedColumnName = "id"))
    private List<Review> reviews;

    // bi-directional many-to-one association to ProjectPictures
    @OneToMany(mappedBy="project", cascade=CascadeType.ALL)
    @JsonManagedReference(value="project-projectPictures")
    private List<ProjectPictures> projectPictures;

    // bi-directional many-to-one association to ProjectLikes
    @OneToMany(mappedBy="project", cascade=CascadeType.ALL)
    @JsonManagedReference(value="project-projectLikes")
    private List<ProjectLikes> projectLikes;

    @ManyToOne
    @JoinColumn(name = "project_type")
    private ProjectType projectType;

    private Boolean enabled;
}
