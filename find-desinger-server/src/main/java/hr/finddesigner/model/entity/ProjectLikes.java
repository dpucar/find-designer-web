package hr.finddesigner.model.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
public class ProjectLikes implements Serializable{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long id;

    private boolean isLiked;

    @Column(name = "user_id")
    private Long userId;

    // bi-directional many-to-one association to Project
    @ManyToOne
    @JoinColumn(name="project_id", nullable=false)
    @JsonBackReference(value="project-projectLikes")
    private Project project;
}
