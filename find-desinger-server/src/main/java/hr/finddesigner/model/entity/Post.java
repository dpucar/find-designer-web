package hr.finddesigner.model.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;


@Entity
@Data
@NoArgsConstructor
public class Post implements Serializable{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long id;

    private String title;

    private String text;

    @Column(name = "date_created")
    private Date dateCreated;

    @Column(name = "project_id")
    private Long projectId;
}
