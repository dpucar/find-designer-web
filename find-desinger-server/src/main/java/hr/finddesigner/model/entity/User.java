package hr.finddesigner.model.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
public class User implements Serializable{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    private String surname;

    private String email;

    private String password;

    @Column(name = "user_picture_link")
    private String userPicture;

    @Column(name = "date_created_profile")
    private Date dateCreated;

    @Column(name = "last_usage")
    private Date lastUsage;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "user_roles", joinColumns = @JoinColumn(name = "user_id",
            referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
    private List<Role> roles;

    @OneToMany(mappedBy="user", cascade=CascadeType.ALL)
    @JsonManagedReference(value="user-userSkills")
    private List<UserSkills> userSkills;

    @Column(nullable = false)
    private boolean enabled;
}
