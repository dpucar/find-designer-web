package hr.finddesigner.model.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
public class ProjectPictures implements Serializable{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long id;

    private String link;

    //bi-directional many-to-one association to Project
    @ManyToOne
    @JoinColumn(name="project_id", nullable=false)
    @JsonBackReference(value="project-projectPictures")
    private Project project;

}
