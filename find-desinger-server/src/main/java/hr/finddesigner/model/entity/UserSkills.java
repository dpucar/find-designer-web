package hr.finddesigner.model.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
@Table(name = "skills")
public class UserSkills implements Serializable{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    //bi-directional many-to-one association to User
    @ManyToOne
    @JoinColumn(name="user_id", nullable=false)
    @JsonBackReference(value="user-userSkills")
    private User user;

}
