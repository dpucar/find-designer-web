package hr.finddesigner.model.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
@Table(name = "project_reviews")
public class ProjectReview implements Serializable{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long id;

    //bi-directional many-to-one association to Project
    @ManyToOne
    @JoinColumn(name="project_id", nullable=false)
    private Project project;

    //bi-directional many-to-one association to Review
    @ManyToOne
    @JoinColumn(name="review_id")
    private Review review;

}
