package hr.finddesigner.controller;

import hr.finddesigner.model.entity.Review;
import hr.finddesigner.service.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/api/reviews")
public class ReviewController {
    private final ReviewService reviewService;

    @Autowired
    public ReviewController(ReviewService reviewService) {
        this.reviewService = reviewService;
    }

    @GetMapping
    public Iterable<Review> findAll(){
        return reviewService.findAll();
    }

    @PostMapping
    public Review saveReview(@RequestBody Review review){
        return reviewService.saveReview(review);
    }
}
