package hr.finddesigner.controller;

import hr.finddesigner.model.dto.DisplayMessageList;
import hr.finddesigner.model.entity.Message;
import hr.finddesigner.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class MessageController {
    private final MessageService messageService;

    @Autowired
    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @GetMapping(value = "/messages")
    public Iterable<Message> getAllMessages() {
        return messageService.findAll();
    }

    @GetMapping(value = "/messages/chatroom/{chatroom}")
    public Iterable<Message> getChatFromChatroom(@PathVariable("chatroom") String chatroom){
        return messageService.findByChatroomOrderByCreatedAtAsc(chatroom);
    }

    @GetMapping(value = "/checkIfChatroomExists/{senderId}/{recieverId}")
    public Message checkIfChatroomExists(@PathVariable(name = "senderId") Long senderId, @PathVariable(name = "recieverId") Long recieverId){
        return messageService.checkIfChatroomExists(senderId, recieverId);
    }

    @GetMapping(value = "/getMessagesForUser/{senderId}")
    public List<DisplayMessageList> getMessagesForUser(@PathVariable(value = "senderId") Long id){
        return messageService.getMessagesForUser(id);
    }
}