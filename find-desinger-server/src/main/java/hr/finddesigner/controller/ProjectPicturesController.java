package hr.finddesigner.controller;

import hr.finddesigner.model.entity.ProjectPictures;
import hr.finddesigner.service.ProjectPicturesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/api/projectPicutres")
public class ProjectPicturesController {
    private final ProjectPicturesService projectPicturesService;

    @Autowired
    public ProjectPicturesController(ProjectPicturesService projectPicturesService) {
        this.projectPicturesService = projectPicturesService;
    }

    @PostMapping(value = "picture")
    public ProjectPictures savePicture(@RequestBody ProjectPictures projectPictures){
        return projectPicturesService.saveProjectPicture(projectPictures);
    }

    @PostMapping(value = "pictures")
    public Iterable<ProjectPictures> savePictures(@RequestBody Iterable<ProjectPictures> projectPictures){
        return projectPicturesService.saveProjectPictures(projectPictures);
    }
}
