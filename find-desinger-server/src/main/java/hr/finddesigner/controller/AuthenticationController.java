package hr.finddesigner.controller;

import hr.finddesigner.model.dto.UserDTO;
import hr.finddesigner.model.entity.User;
import hr.finddesigner.service.UserService;
import hr.finddesigner.service.impl.UserAuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collection;

@RestController
public class AuthenticationController {
    private final UserService userService;
    private final LogoutHandler logoutHandler;
    private final UserAuthService userAuthService;

    @Autowired
    public AuthenticationController(UserService userService, LogoutHandler logoutHandler, UserAuthService userAuthService) {
        this.userService = userService;
        this.logoutHandler = logoutHandler;
        this.userAuthService = userAuthService;
    }

    @PostMapping("/api/login")
    public User login(@RequestBody UserDTO user) {
        User loggedInUser = new User();
        try {
            Authentication authenticate = userAuthService.authenticate(user.getEmail(), user.getPassword());
            SecurityContextHolder.getContext().setAuthentication(authenticate);

            if(authenticate.isAuthenticated()){
                loggedInUser = userService.findByEmail(user.getEmail());
            }else{
                loggedInUser.setId(-1L);
            }

            return loggedInUser;
        } catch (BadCredentialsException ex) {
            System.out.println("Bad credentials");
            loggedInUser.setId(-1L);
            return loggedInUser;
        } catch (Exception ex) {
            loggedInUser.setId(-1L);
            return loggedInUser;
        }
    }

    @PostMapping(value = "/api/loginDefaultUser")
    public User authenticateDefaultUser(@RequestBody User user){
        User u = userService.findByEmail("designer@mail.com");
        try {
            Authentication authentication = userAuthService.authenticate("designer@mail.com", "pass");
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return u;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return u;
    }

    @GetMapping(value = "/api/isLoggedIn")
    public User isUserLoggedIn() {
        User us = new User();
        us.setId(-1L);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if(authentication != null && authentication.isAuthenticated()
                && !(SecurityContextHolder.getContext().getAuthentication() instanceof AnonymousAuthenticationToken)){
            us = userService.findByEmail(authentication.getName());
        }
        return us;
    }


    @PostMapping(value = "/api/logout")
    public User logout(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            logoutHandler.logout(request, response, auth);
        }
        return new User();
    }

    @GetMapping(value = "/api/authority")
    public Collection<SimpleGrantedAuthority> getAuth(){
        return (Collection<SimpleGrantedAuthority>) SecurityContextHolder.getContext().getAuthentication().getAuthorities();
    }
}
