package hr.finddesigner.controller;

import hr.finddesigner.model.entity.User;
import hr.finddesigner.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/api/users")
public class UserController {
    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public Iterable<User> getAllUsers(){
        return userService.getAllUsers();
    }

    @GetMapping(value = "{id}")
    public User getUserById(@PathVariable Long id){
        return userService.getUserById(id);
    }

    @PostMapping
    public User updateUser(@RequestBody User user){
        return userService.updateUser(user);
    }

    @PostMapping(value = "/register/{roleName}")
    public User registerUser(@RequestBody User user, @PathVariable(name = "roleName") String roleName){
        return userService.registerUserAndRole(user, roleName);
    }

    @PostMapping(value = "/byemail")
    public Boolean doesEmailExists(@RequestBody User user){
        return userService.doesUserEmailExists(user.getEmail());
    }
}
