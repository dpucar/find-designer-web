package hr.finddesigner.controller;

import hr.finddesigner.model.entity.Project;
import hr.finddesigner.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/projects")
public class ProjectController {
    private final ProjectService projectService;

    @Autowired
    public ProjectController(ProjectService projectService) {
        this.projectService = projectService;
    }

    @GetMapping(value = "{id}")
    public Project getProjectById(@PathVariable Long id){
        return projectService.getProjectById(id);
    }

    @GetMapping
    public Iterable<Project> getAllProjects(){
        return projectService.getProjects();
    }

    @PostMapping
    public Project saveProject(@RequestBody Project project){
        return projectService.saveProject(project);
    }

    @PutMapping(value = "{id}")
    public Project updateProject(@PathVariable(name = "id")Long id, @RequestBody Project project){
        return projectService.updateProject(id, project);
    }
}
