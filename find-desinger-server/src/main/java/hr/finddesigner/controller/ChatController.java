package hr.finddesigner.controller;

import hr.finddesigner.model.entity.Message;
import hr.finddesigner.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@Controller
public class ChatController {

    private final SimpMessagingTemplate simpMessagingTemplate;
    private final MessageService messageService;

    @Autowired
    public ChatController(SimpMessagingTemplate simpMessagingTemplate, MessageService messageService) {
        this.simpMessagingTemplate = simpMessagingTemplate;
        this.messageService = messageService;
    }

    @MessageMapping("/chat/{chatroom}")
    public Message simple(@DestinationVariable String chatroom, final Message m) {
        Message message = messageService.saveChat(m);
        simpMessagingTemplate.convertAndSend("/topic/fleet/" + chatroom, m);

        return message;
    }
}
