package hr.finddesigner.controller;

import hr.finddesigner.model.entity.Project;
import hr.finddesigner.model.entity.UserProject;
import hr.finddesigner.service.UserProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/api/userProjects")
@CrossOrigin
@RestController
public class UserProjectController {
    private final UserProjectService userProjectService;

    @Autowired
    public UserProjectController(UserProjectService userProjectService) {
        this.userProjectService = userProjectService;
    }

    @GetMapping
    public Iterable<UserProject> findAllUserProjects(){
        return userProjectService.findAllUserProjects();
    }

    @GetMapping(value = "{id}")
    public Iterable<Project> getProjectsByUserId(@PathVariable Long id){
        return userProjectService.getProjectsByUserId(id);
    }

    @GetMapping(value = "/byproject/{id}")
    public UserProject getUserProjectByProjectId(@PathVariable(value = "id") Long id){
        return userProjectService.findUserProjectByProjectId(id);
    }

    @GetMapping(value = "/byuser/{id}")
    public Iterable<UserProject> getUserProjectByUserId(@PathVariable(value = "id") Long id){
        return userProjectService.findAllByUserId(id);
    }

    @PostMapping
    public UserProject saveUserProject(@RequestBody UserProject userProject){
        return userProjectService.saveUserProject(userProject);
    }
}
