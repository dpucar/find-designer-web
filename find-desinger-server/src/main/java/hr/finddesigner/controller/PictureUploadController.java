package hr.finddesigner.controller;

import hr.finddesigner.service.ImageUploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/pictureUpload")
public class PictureUploadController {

    private final ImageUploadService imageUploadService;

    @Autowired
    public PictureUploadController(ImageUploadService imageUploadService) {
        this.imageUploadService = imageUploadService;
    }

    @PostMapping(value="profilePicture")
    public String saveProfilePictures(@RequestPart("file") MultipartFile file) {
        return createProfilePicture(file);
    }

    @PostMapping(value = "projectPicture")
    public String saveProjectPictures(@RequestPart("file") MultipartFile file){
        return createProfilePicture(file);
    }

    private String createProfilePicture(MultipartFile image) {
        try {
            return imageUploadService.save(image.getBytes(), image.getOriginalFilename());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}