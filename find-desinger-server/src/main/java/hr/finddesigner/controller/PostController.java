package hr.finddesigner.controller;

import hr.finddesigner.model.entity.Post;
import hr.finddesigner.model.entity.UserPosts;
import hr.finddesigner.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/posts")
public class PostController {
    private final PostService postService;

    @Autowired
    public PostController(PostService postService) {
        this.postService = postService;
    }

    @GetMapping
    public Iterable<Post> findAllPosts(){
        return postService.findAllPosts();
    }

    @PostMapping(value = "/post")
    public Post savePost(@RequestBody Post post){
        return postService.savePost(post);
    }

    @GetMapping(value = "/userPosts")
    public Iterable<UserPosts> findAllUserPosts(){
        return postService.findAllUserPosts();
    }

    @PostMapping(value = "/userPosts")
    public UserPosts saveUserPost(@RequestBody UserPosts userPost){
        return postService.saveUserPost(userPost);
    }
}
