package hr.finddesigner.controller;

import hr.finddesigner.model.entity.ProjectReview;
import hr.finddesigner.model.entity.Review;
import hr.finddesigner.service.ProjectReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/projectreviews")
public class ProjectReviewController {
    private final ProjectReviewService projectReviewService;

    @Autowired
    public ProjectReviewController(ProjectReviewService projectReviewService) {
        this.projectReviewService = projectReviewService;
    }

    @PostMapping
    public ProjectReview saveProjectReview(@RequestBody ProjectReview projectReview){
        return projectReviewService.saveProjectReview(projectReview);
    }

    @GetMapping(value = "{id}")
    public List<Review> findReviewsByProjectId(@PathVariable(value = "id") Long id){
        return projectReviewService.findByProjectId(id);
    }
}
