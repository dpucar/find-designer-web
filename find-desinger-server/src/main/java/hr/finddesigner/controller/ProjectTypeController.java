package hr.finddesigner.controller;

import hr.finddesigner.model.entity.ProjectType;
import hr.finddesigner.service.ProjectTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(value = "/api/projectType")
public class ProjectTypeController {
    private final ProjectTypeService projectTypeService;

    @Autowired
    public ProjectTypeController(ProjectTypeService projectTypeService) {
        this.projectTypeService = projectTypeService;
    }

    @GetMapping
    public List<ProjectType> getAllProjectTypes(){
        return projectTypeService.getAllProjectTypes();
    }
}
