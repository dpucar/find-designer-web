package hr.finddesigner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;

@SpringBootApplication
public class FindDesingerServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(FindDesingerServerApplication.class, args);
	}

	@Bean
	public LogoutHandler logoutHandler(){
		return new SecurityContextLogoutHandler();
	}
}
